<?php

// Register Custom Post Type
function mwt_dukungan_post_type() {

	$labels = array(
		'name'                  => _x( 'Data Dukungan', 'Post Type General Name', 'mwt' ),
		'singular_name'         => _x( 'Dukungan', 'Post Type Singular Name', 'mwt' ),
		'menu_name'             => __( 'Dukungan', 'mwt' ),
		'name_admin_bar'        => __( 'Dukungan', 'mwt' ),
		'archives'              => __( 'Item Archives', 'mwt' ),
		'attributes'            => __( 'Item Attributes', 'mwt' ),
		'parent_item_colon'     => __( 'Parent Item:', 'mwt' ),
		'all_items'             => __( 'All Items', 'mwt' ),
		'add_new_item'          => __( 'Add New Item', 'mwt' ),
		'add_new'               => __( 'Add New', 'mwt' ),
		'new_item'              => __( 'New Item', 'mwt' ),
		'edit_item'             => __( 'Edit Item', 'mwt' ),
		'update_item'           => __( 'Update Item', 'mwt' ),
		'view_item'             => __( 'View Item', 'mwt' ),
		'view_items'            => __( 'View Items', 'mwt' ),
		'search_items'          => __( 'Search Item', 'mwt' ),
		'not_found'             => __( 'Not found', 'mwt' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'mwt' ),
		'featured_image'        => __( 'Featured Image', 'mwt' ),
		'set_featured_image'    => __( 'Set featured image', 'mwt' ),
		'remove_featured_image' => __( 'Remove featured image', 'mwt' ),
		'use_featured_image'    => __( 'Use as featured image', 'mwt' ),
		'insert_into_item'      => __( 'Insert into item', 'mwt' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'mwt' ),
		'items_list'            => __( 'Items list', 'mwt' ),
		'items_list_navigation' => __( 'Items list navigation', 'mwt' ),
		'filter_items_list'     => __( 'Filter items list', 'mwt' ),
	);
	$args = array(
		'label'                 => __( 'Dukungan', 'mwt' ),
		'description'           => __( 'Dukungan Description', 'mwt' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'revisions' ),
		'taxonomies'            => array( 'polres' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => false,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-flag',
		'show_in_admin_bar'     => false,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'dukungan', $args );

}
add_action( 'init', 'mwt_dukungan_post_type', 0 );


// Register Custom Taxonomy
function polres_dukungan_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Polres', 'Taxonomy General Name', 'mwt' ),
		'singular_name'              => _x( 'Polres', 'Taxonomy Singular Name', 'mwt' ),
		'menu_name'                  => __( 'Polres', 'mwt' ),
		'all_items'                  => __( 'All Items', 'mwt' ),
		'parent_item'                => __( 'Parent Item', 'mwt' ),
		'parent_item_colon'          => __( 'Parent Item:', 'mwt' ),
		'new_item_name'              => __( 'New Item Name', 'mwt' ),
		'add_new_item'               => __( 'Add New Item', 'mwt' ),
		'edit_item'                  => __( 'Edit Item', 'mwt' ),
		'update_item'                => __( 'Update Item', 'mwt' ),
		'view_item'                  => __( 'View Item', 'mwt' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'mwt' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'mwt' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'mwt' ),
		'popular_items'              => __( 'Popular Items', 'mwt' ),
		'search_items'               => __( 'Search Items', 'mwt' ),
		'not_found'                  => __( 'Not Found', 'mwt' ),
		'no_terms'                   => __( 'No items', 'mwt' ),
		'items_list'                 => __( 'Items list', 'mwt' ),
		'items_list_navigation'      => __( 'Items list navigation', 'mwt' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'polres', array( 'dukungan' ), $args );

}
add_action( 'init', 'polres_dukungan_taxonomy', 0 );
