<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Adinaz
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div class="row">
        <div class="col-md-10 ml-auto mr-auto">
						<header class="entry-header">
								<?php

								if ( 'post' === get_post_type() ) : ?>
								<!-- <h3 class="title-uppercase"><small>Written by designers for designers</small></h3> -->
								<div class="entry-meta">
									<h6 class="title-uppercase"><?php mwt_posted_on(); ?></h6>
								</div><!-- .entry-meta -->
								<?php
								endif; ?>
						</header><!-- .entry-header -->
        </div>
        <div class="col-md-10 ml-auto mr-auto">
            <a href="<?php echo get_the_permalink(); ?>">
                <div class="card" data-radius="none" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>');"></div>
            </a>
            <div class="entry-content article-content">
							<?php
								the_content( sprintf(
									wp_kses(
										/* translators: %s: Name of current post. Only visible to screen readers */
										__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'adinaz' ),
										array(
											'span' => array(
												'class' => array(),
											),
										)
									),
									get_the_title()
								) );

								wp_link_pages( array(
									'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'adinaz' ),
									'after'  => '</div>',
								) );
							?>
            </div>
            <br>
						<footer class="entry-footer article-footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <?php mwt_entry_footer(); ?>
                        </div>
                        <div class="col-md-4 ml-auto">
													<!--
                            <div class="sharing">
                                <h5>Spread the word</h5>
                                <button class="btn btn-just-icon btn-twitter">
                                    <i class="fa fa-twitter"></i>
                                </button>
                                <button class="btn btn-just-icon btn-facebook">
                                    <i class="fa fa-facebook"> </i>
                                </button>
                                <button class="btn btn-just-icon btn-google">
                                    <i class="fa fa-google"> </i>
                                </button>
                            </div>
													-->
                        </div>
                    </div>
                </div>
						</footer><!-- .entry-footer -->
        </div>
    </div>
</article><!-- #post-<?php the_ID(); ?> -->
