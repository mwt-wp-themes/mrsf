jQuery( document ).ready( function($) {

   /**
    *
    * strict mode
    * 
    */               

    'use strict';
    
   /**
    *
    * global variables
    *
    */                
    
    var windowWidth = 0;
    var windowHeight = 0;
    var ajax_url = mrsf_object.ajax_url;
  
   /**
    *
    * Link Scroll Effects
    *
    */  
    // Select all links with hashes
    $('a[href*="#"]')
      // Remove links that don't actually link to anything
      .not('[href="#"]')
      .not('[href="#0"]')
      .click(function(event) {
        // On-page links
        if (
          location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
          && 
          location.hostname == this.hostname
        ) {
          // Figure out element to scroll to
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
          // Does a scroll target exist?
          if (target.length) {
            // Only prevent default if animation is actually gonna happen
            event.preventDefault();
            $('html, body').animate({
              scrollTop: target.offset().top
            }, 1000, function() {
              // Callback after animation
              // Must change focus!
              var $target = $(target);
              $target.focus();
              if ($target.is(":focus")) { // Checking if the target was focused
                return false;
              } else {
                $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                $target.focus(); // Set focus again
              };
            });
          }
        }
      });
  
   /**
    *
    * Deklarsi form action
    * 
    */                

    $.mrsfDeklarasiAction = function(form) {
      $("#form-msg").html('');
      $(form).LoadingOverlay("show");
      var formData = $(form).serialize();
      $.ajax({
        type: 'POST',
        url: ajax_url + '?action=',
        data: formData + "&" + $.param({wp_nonce:mrsf_object.wp_nonce, action: 'mrsf_submit_deklarasi'}),
      })
      .done(function(response) {
        var data = JSON.parse(response);
        $("#nama").val('');
        $("#usia").val('');
        $("#no_hp").val('');
        $("#email").val('');
        $("#polres").val('');
        $("#form-msg").html('<div class="alert alert-success">'+data.msg+'</div>');
        $(form).LoadingOverlay("hide");
      })
      .fail(function(data) {
        $(form).LoadingOverlay("hide");
        if (data.responseText !== '') {
          alert(data.responseText);
        } else {
          alert('Oops! An error occured and your message could not be sent.');
        }
      });         
    };
  
   /**
    *
    * Chart
    * 
    */  
  
   /**
    *
    * init chart
    * 
    */
    $.mrsfInitStatistikChart = function() {
      $.ajax({
        type: 'POST',
        url: ajax_url,
        data: {
          action: 'mrsf_get_data_statistik_dukungan'
        }
      })
      .done(function(response) {
        var data = JSON.parse(response);
        var result = data.data;

//         $("#form-msg").text(data.msg);
//         $(form).LoadingOverlay("hide");
        
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(function() {

          var data = google.visualization.arrayToDataTable(result.polres);

          var options = {
            title: 'Dukungan Polres',
            is3D: true,
            backgroundColor: { fill:'transparent' }
          };

          var chart = new google.visualization.PieChart(document.getElementById('dukunganChart'));

          chart.draw(data, options);
        });
        google.charts.setOnLoadCallback(function() {

          var data = google.visualization.arrayToDataTable(result.usia);

          var options = {
            title: 'Usia Millenial',
            is3D: true,
            backgroundColor: { fill:'transparent' }
          };

          var chart = new google.visualization.PieChart(document.getElementById('usiaChart'));

          chart.draw(data, options);
        });
        
      })
      .fail(function(data) {
        // Set the message text.
        if (data.responseText !== '') {
          alert(data.responseText);
        } else {
          alert('Oops! An error occured and your message could not be sent.');
        }
      }); 
    }
  
  
  
   /**
    *
    * end of file
    * 
    */                

});