<?php

Redux::setSection( $opt_name, array(
    'title'  => __( 'Homepage', 'mwt' ),
    'id'    => 'mwt-homepage-option',
    'desc'  => __( 'Basic fields as subsections.', 'mwt' ),
    'icon'  => 'el el-home'
) );

Redux::setSection( $opt_name, array(
    'title'  => __( 'Section: Hero', 'mwt' ),
    'id'     => 'mwt-hero-options',
    'subsection' => true,
    'fields' => array(
        array(
            'id'       => 'hero_type',
            'type'     => 'select',
            'title'    => __('Hero Type', 'mwt'), 
            'options'  => array(
                'banner' => __('Banner Image', 'mwt'), 
                'video' => __('Video', 'mwt'), 
                //'sliders' => __('Sliders', 'mwt'), 
            ),
            'default' => 'banner'
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title'  => __( '- Hero Banner', 'mwt' ),
    'desc'  => __( 'Hero Banner Section', 'mwt' ),
    'id'     => 'mwt-hero-banner-options',
    'subsection' => true,
    'fields' => array(
        array(
            'id'       => 'banner_bg_image',
            'type'     => 'media',
            'title'    => __( 'Banner Image', 'mwt' ),
            'url'      => true,
            'preview'  => true,
        ),
        array(
            'id'       => 'banner_title',
            'type'     => 'text',
            'title'    => __( 'Title', 'mwt' ),
        ),
        array(
            'id'       => 'banner_subtitle',
            'type'     => 'text',
            'title'    => __( 'Subtitle', 'mwt' ),
        ),
        array(
            'id'       => 'banner_description',
            'type'     => 'textarea',
            'title'    => __( 'Description', 'mwt' ),
        ),
        array(
            'id'       => 'banner_size',
            'type'     => 'select',
            'title'    => __( 'Size', 'mwt' ),
            'options'  => array(
                'large' => __('Large (Fullscreen)', 'mwt'), 
                'small' => __('Medium', 'mwt'), 
                'xs' => __('Small', 'mwt'), 
            ),
            'default' => 'center'
        ),
        array(
            'id'       => 'banner_alignment',
            'type'     => 'select',
            'title'    => __( 'Alignment', 'mwt' ),
            'options'  => array(
                'center' => __('Center', 'mwt'), 
                'left' => __('Left', 'mwt'), 
                'right' => __('Right', 'mwt'), 
            ),
            'default' => 'center'
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title'  => __( '- Hero Video', 'mwt' ),
    'desc'  => __( 'Video Section', 'mwt' ),
    'id'     => 'mwt-hero-video-options',
    'subsection' => true,
    'fields' => array(
        array(
            'id'       => 'video_url',
            'type'     => 'text',
            'title'    => __( 'Video URL', 'mwt' ),
        ),
        array(
            'id'       => 'video_title',
            'type'     => 'text',
            'title'    => __( 'Title', 'mwt' ),
        ),
        array(
            'id'       => 'video_subtitle',
            'type'     => 'text',
            'title'    => __( 'Subtitle', 'mwt' ),
        ),
        array(
            'id'       => 'video_description',
            'type'     => 'textarea',
            'title'    => __( 'Description', 'mwt' ),
        ),
    )
) );

// Redux::setSection( $opt_name, array(
//     'title'  => __( '- Hero Sliders', 'mwt' ),
//     'desc'  => __( 'Slider/Banner Section', 'mwt' ),
//     'id'     => 'mwt-hero-slider-options',
//     'subsection' => true,
//     'fields' => array(
//         array(
//             'id'          => 'home_slides',
//             'type'        => 'slides',
//             'title'       => __('Slides Options', 'mwt'),
//             'subtitle'    => __('Unlimited slides with drag and drop sortings.', 'mwt'),
//             'placeholder' => array(
//                 'title'           => __('This is a title', 'mwt'),
//                 'description'     => __('Description Here', 'mwt'),
//                 'url'             => __('Give us a link!', 'mwt'),
//             ),
//         ),
//         array(
//             'id'       => 'slide_alignment',
//             'type'     => 'radio',
//             'title'    => __('Slider Alignment', 'mwt'), 
//             'options'  => array(
//                 'left' => __('Left', 'mwt'), 
//                 'center' => __('Center', 'mwt'), 
//                 'right' => __('Right', 'mwt'), 
//             ),
//             'default' => 'center'
//         ),
//         array(
//             'id'       => 'slide_button_text',
//             'type'     => 'text',
//             'title'    => __('Button Text', 'mwt'), 
//             'default' => 'Read More'
//         ),
//     )
// ) );

Redux::setSection( $opt_name, array(
    'title'  => __( 'Section: Roll Out', 'mwt' ),
    'id'     => 'mwt-roll-out-section-options',
    'subsection' => true,
    'fields' => array(
        array(
            'id'       => 'enable_rollout',
            'type'     => 'radio',
            'title'    => __('Enable Section', 'mwt'), 
            'options'  => array(
                '1' => __('Yes', 'mwt'), 
                '2' => __('No', 'mwt'), 
            ),
            'default' => '2'
        ),
        array(
            'id'       => 'rollout_bg_image',
            'type'     => 'media',
            'title'    => __( 'Background Image', 'mwt' ),
            'url'      => true,
            'preview'  => true,
        ),
        array(
            'id'       => 'rollout_title',
            'type'     => 'textarea',
            'title'    => __('Title', 'mwt'), 
            'default' => 'About Us'
        ),
        array(
            'id'       => 'rollout_subtitle',
            'type'     => 'textarea',
            'title'    => __('Subtitle', 'mwt'),
        ),
        array(
            'id'       => 'rollout_state_1',
            'type'     => 'textarea',
            'title'    => __('Stage 1', 'mwt'),
        ),
        array(
            'id'       => 'rollout_state_2',
            'type'     => 'textarea',
            'title'    => __('Stage 2', 'mwt'),
        ),
        array(
            'id'       => 'rollout_state_3',
            'type'     => 'textarea',
            'title'    => __('Stage 3', 'mwt'),
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title'  => __( 'Section: Info/About', 'mwt' ),
    'id'     => 'mwt-info-options',
    'subsection' => true,
    'fields' => array(
        array(
            'id'       => 'enable_info',
            'type'     => 'radio',
            'title'    => __('Enable Info Area', 'mwt'), 
            'options'  => array(
                '1' => __('Yes', 'mwt'), 
                '2' => __('No', 'mwt'), 
            ),
            'default' => '2'
        ),
        array(
            'id'       => 'info_bg_image',
            'type'     => 'media',
            'title'    => __( 'Background Image', 'mwt' ),
            'url'      => true,
            'preview'  => true,
        ),
        array(
            'id'       => 'info_title',
            'type'     => 'text',
            'title'    => __('Title', 'mwt'), 
            'default' => ''
        ),
        array(
            'id'       => 'info_subtitle',
            'type'     => 'text',
            'title'    => __('Subtitle', 'mwt'), 
            'default' => ''
        ),
        array(
            'id'       => 'info_description',
            'type'     => 'editor',
            'title'    => __('Description', 'mwt'),
            'args'   => array(
                'teeny'           => false,
                'tinymce'         => true,
                'wpautop'          => false
                // 'textarea_rows'    => 10
            )
        ),
        array(
            'id'       => 'info_items',
            'type'     => 'multi_text',
            'title'    => __('Items', 'mwt'),
        ),
        array(
            'id'       => 'info_image_show',
            'type'     => 'radio',
            'title'    => __('Show Info Images', 'redux-framework-demo'), 
            'options'  => array(
                '1' => 'Yes', 
                '2' => 'No'
            ),
            'default' => '2'
        ),
        array(
            'id'       => 'info_image_1',
            'type'     => 'media',
            'title'    => __( 'Info Image 1', 'mwt' ),
            'url'      => true,
            'preview'  => true,
        ),
        array(
            'id'       => 'info_image_2',
            'type'     => 'media',
            'title'    => __( 'Info Image 2', 'mwt' ),
            'url'      => true,
            'preview'  => true,
        ),
        array(
            'id'       => 'info_image_3',
            'type'     => 'media',
            'title'    => __( 'Info Image 3', 'mwt' ),
            'url'      => true,
            'preview'  => true,
        ),
        array(
            'id'       => 'info_image_size',
            'type'     => 'dimensions',
            'units'    => array('px','%'),
            'title'    => __('Info Image Size', 'mwt'), 
        ),
        array(
            'id'       => 'info_btn_text',
            'type'     => 'text',
            'title'    => __('Button Text', 'mwt'), 
            'default' => ''
        ),
        array(
            'id'       => 'info_btn_url',
            'type'     => 'text',
            'title'    => __('Button Link', 'mwt'), 
            'default' => '#'
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title'  => __( 'Section: Form', 'mwt' ),
    'id'     => 'mwt-contact-us-options',
    'subsection' => true,
    'fields' => array(
        array(
            'id'       => 'enable_form',
            'type'     => 'radio',
            'title'    => __('Enable Form', 'mwt'), 
            'options'  => array(
                '1' => __('Yes', 'mwt'), 
                '2' => __('No', 'mwt'), 
            ),
            'default' => '2'
        ),
        array(
            'id'       => 'form_bg_image',
            'type'     => 'media',
            'title'    => __( 'Banner Image', 'mwt' ),
            'url'      => true,
            'preview'  => true,
        ),
        array(
            'id'       => 'form_title',
            'type'     => 'text',
            'title'    => __('Title', 'mwt'), 
            'default' => ''
        ),
        array(
            'id'       => 'form_usia_min',
            'type'     => 'text',
            'validate' => 'numeric',
            'title'    => __('Usia Min', 'mwt'), 
            'default' => '17'
        ),
        array(
            'id'       => 'form_usia_max',
            'type'     => 'text',
            'validate' => 'numeric',
            'title'    => __('Usia Max', 'mwt'), 
            'default' => '29'
        ),
        array(
            'id'       => 'form_success_message',
            'type'     => 'textarea',
            'title'    => __('Submission Message', 'mwt'), 
            'desc'     => __('Pesan setelah visitor men-submit form.', 'mwt'),
            'default' => 'Sukses!'
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title'  => __( 'Section: FAQ', 'mwt' ),
    'desc'  => __( 'Lihat semua FAQ di halaman <a href="edit.php?post_type=faqs">FAQs</a>. Untuk menambahkan faq, silahkan ke halaman <a href="post-new.php?post_type=faqs">tambah faqs</a>.', 'mwt' ),
    'id'     => 'mwt-faqs-options',
    'subsection' => true,
    'fields' => array(
        array(
            'id'       => 'enable_faqs',
            'type'     => 'radio',
            'title'    => __('Enable FAQs', 'mwt'), 
            'options'  => array(
                '1' => __('Yes', 'mwt'), 
                '2' => __('No', 'mwt'), 
            ),
            'default' => '2'
        ),
        array(
            'id'       => 'faqs_title',
            'type'     => 'text',
            'title'    => __('Title', 'mwt'), 
            'default' => 'FAQ'
        ),
    )
) );

Redux::setSection( $opt_name, array(
    'title'  => __( 'Section: Blog', 'mwt' ),
    'desc'  => __( 'Blog Section', 'mwt' ),
    'id'     => 'mwt-recent-blog-options',
    'subsection' => true,
    'fields' => array(
        array(
            'id'       => 'enable_recent_blog',
            'type'     => 'radio',
            'title'    => __('Enable Blog', 'mwt'), 
            'options'  => array(
                '1' => __('Yes', 'mwt'), 
                '2' => __('No', 'mwt'), 
            ),
            'default' => '2'
        ),
        array(
            'id'       => 'recent_blog_title',
            'type'     => 'text',
            'title'    => __('Title', 'mwt'), 
            'default' => 'Latest News'
        ),
        array(
            'id'       => 'recent_blog_numberposts',
            'type'     => 'text',
            'title'    => __('Number of post to show', 'mwt'), 
            'default' => 3,
            'validate'  => 'numeric'
        ),
    )
) );