<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package rensya
 */
global $mwt_options;
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('col-sm-12 wow fadeIn'); ?> data-wow-duration="2s" data-wow-offset="25">
	
		<div class="card">
			<div class="card-header" id="heading<?php the_ID(); ?>">
				<h5 class="mb-0">
					<button class="btn btn-link" data-toggle="collapse" data-target="#collapse<?php the_ID(); ?>" aria-expanded="false" aria-controls="collapse<?php the_ID(); ?>"><?php the_title(); ?></button>
				</h5>
			</div>
			<div id="collapse<?php the_ID(); ?>" class="collapse" aria-labelledby="heading<?php the_ID(); ?>" data-parent="#accordion">
				<div class="card-body">
					<?php the_content(); ?>
				</div>
			</div>
		</div>	
	
</article><!-- #post-<?php the_ID(); ?> -->
