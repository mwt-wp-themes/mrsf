<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package rensya
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="utf-8" />
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	<meta name="viewport" content="width=device-width" />

	<?php wp_head(); 
	global $mwt_options;?>
	
	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/wow.min.js"></script>
	<script>
	new WOW().init();
	</script>

</head>

<body <?php body_class(); ?>>

  <div id="masthead" class="header-wrapper">
    <?php mwt_navigation(); ?>
    <?php ( is_front_page() ) ? mwt_front_page_header() : mwt_page_header(); ?>
  </div>

	<div id="content" class="wrapper">
    