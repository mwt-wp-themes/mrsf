<?php 
	global $mwt_options;
	if( $mwt_options['enable_form'] == 1 ) : 
  $bg_image = ( isset($mwt_options['form_bg_image']['url']) && $mwt_options['form_bg_image']['url'] != '' ) ? $mwt_options['form_bg_image']['url'] : get_template_directory_uri() . '/assets/img/contact-bg.jpg';
	?>
<div id="section-form" class="section section-image" style="background-image: url('<?php echo $bg_image; ?>')">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-5">
        <?php if( !empty( $mwt_options['form_title'] ) ) : ?>
        <h4 class="title"><?php echo $mwt_options['form_title']; ?></h4>
        <?php endif; ?>
        <div id="form-msg"></div>
				<form id="deklarasi-form" action="<?php echo admin_url('admin_ajax.php'); ?>" method="post">
					<div class="form-group row align-items-center">
						<label for="nama" class="col-sm-3 col-form-label">Nama</label>
						<div class="col-sm-9">
							<input type="text" class="form-control custom-input" id="nama" name="nama" required>
						</div>
					</div>
					<div class="form-group row align-items-center">
						<label for="usia" class="col-sm-3 col-form-label">Usia</label>
						<div class="col-sm-9">
              <select class="form-control" id="usia" name="usia" required size="3">
                <?php 
                for( $i = intval( $mwt_options['form_usia_min'] ); $i <= intval( $mwt_options['form_usia_max'] ); $i++ ) {
                  echo '<option value="' . $i . '">' . $i . '</option>';
                }
                ?>
              </select>
						</div>
					</div>
					<div class="form-group row align-items-center">
						<label for="no_hp" class="col-sm-3 col-form-label">No HP</label>
						<div class="col-sm-9">
							<input type="text" class="form-control custom-input" id="no_hp" name="no_hp" required>
						</div>
					</div>
					<div class="form-group row align-items-center">
						<label for="email" class="col-sm-3 col-form-label">Email</label>
						<div class="col-sm-9">
							<input type="email" class="form-control custom-input" id="email" name="email" required>
						</div>
					</div>
					<div class="form-group">
						<label for="polres">Deklarasi untuk Polres</label>
            <select class="form-control" id="polres" name="polres" required size="5">
<!--               <option value=""></option> -->
              <?php $terms = get_terms( 'polres', array(
    'hide_empty' => false,
) );
              foreach( $terms as $term ) {
                echo '<option value="' . $term->term_id . '">' . $term->name . '</option>';
              }
              ?>
            </select>
					</div>
					<button id="submit-deklarasi" type="submit" class="btn btn-success btn-block">Tandatangan untuk deklarasi ini <span>dan sebarkan tautan</span></button>
				</form>
			</div>
			<div class="col-sm-12 col-md-7 text-center">
        <?php if( isset($mwt_options['logo']['url']) && $mwt_options['logo']['url'] != '' ) : ?>
        <img src="<?php echo $mwt_options['logo']['url']; ?>" class="img-fluid main-logo">     
        <?php endif; ?>
        <h4 class="title">Statistik</h4>
        <div id="dukunganChart"></div>
        <div id="usiaChart"></div>
        <div class="row">
          <div class="col-md-12 text-center partner-logo">
            <?php $partner_gallery = explode( ",", $mwt_options['partner_logo'] ); 
            $count = 1;
            foreach( $partner_gallery as $photo_id ) :
            ?>
              <span class="partner-<?php echo $count; ?>">
                <img src="<?php echo wp_get_attachment_url( $photo_id ); ?>" class="img-fluid">
              </span>
            <?php $count++; endforeach; ?>
          </div>
        </div>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>

