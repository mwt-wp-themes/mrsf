<form class="search-form" role="search" method="get" action="<?php echo home_url( '/' ); ?>">
	<div class="input-group">
	  <input id="s" type="text" class="form-control" name="s" placeholder="<?php echo __('Search', 'mb_grace'); ?>">
	  <span class="input-group-btn">
	    <button class="btn btn-primary" type="submit"><?php echo __('Submit', 'mb-grace'); ?></button>
	  </span>
	</div><!-- /input-group -->
</form>