<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Adinaz
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="row">
					<div class="col-sm-6">
							<?php if( is_singular() ): ?>
								<div class="card" data-radius="none" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>');"></div>
							<?php else: ?>
								<div class="space-top"></div>
								<img src="<?php echo get_the_post_thumbnail_url(); ?>" class="img-rounded img-responsive" />
							<?php endif; ?>
					</div>
					<div class="col-sm-6">
					<?php if( is_singular() ): ?>
						<h2><?php the_title(); ?></h2>
					<?php else: ?>
						<h3><?php the_title(); ?></h3>
					<?php endif; ?>
					<hr>
					<?php
						the_content( sprintf(
							wp_kses(
								/* translators: %s: Name of current post. Only visible to screen readers */
								__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'adinaz' ),
								array(
									'span' => array(
										'class' => array(),
									),
								)
							),
							get_the_title()
						) );

						wp_link_pages( array(
							'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'adinaz' ),
							'after'  => '</div>',
						) );
					?>
					</div>
			</div>
</article><!-- #post-<?php the_ID(); ?> -->
