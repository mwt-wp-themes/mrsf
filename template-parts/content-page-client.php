<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package rensya
 */

global $mwt_options;
?>

<div class="row">
	<div class="col-md-10 ml-auto mr-auto">
		<div id="home-client-carousel" class="owl-carousel owl-theme">
			<?php $client_gallery = explode( ",", $mwt_options['client_gallery'] ); 
			foreach( $client_gallery as $photo_id ) :
			?>
				<div class="item wow swing">
				<img src="<?php echo wp_get_attachment_url( $photo_id ); ?>" height="50">
				</div>
			<?php endforeach; ?>
		</div>
	</div>
</div>
