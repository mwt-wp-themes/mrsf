<?php

Redux::setSection( $opt_name, array(
    'title'  => __( 'Footer', 'mwt' ),
    'id'     => 'mwt-footer-options',
    'icon'   => 'el el-th-list',
    'fields' => array(
        array(
            'id'       => 'copyright-text',
            'type'     => 'textarea',
            'title'    => __( 'Copyright Text', 'mwt' ),
            'desc'     => __( '', 'mwt' ),
            'subtitle' => __( '', 'mwt' ),
        ),
    )
) );