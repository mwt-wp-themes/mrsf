<?php global $mwt_options; if( $mwt_options['enable_recent_blog'] == 1 ) : ?>
<div id="section-blog" class="section section-gray">
    <div class="container">
        <div class="row">
            <div class="col-md-10 ml-auto mr-auto">
                <h2 class="title wow slideInDown"><?php echo $mwt_options['recent_blog_title']; ?></h2>
                <br>
                <div class="row wow zoomIn">
                  <?php $args = array( 'post_type'	=> 'post', 'post_status' => 'publish', 'posts_per_page' => $mwt_options['recent_blog_numberposts'], 'order' => 'DESC' );
                  $the_query = new WP_Query( $args );
                  if ( $the_query->have_posts() ) {
                    while ( $the_query->have_posts() ) {
                      $the_query->the_post();
                      ?>
                      <div class="col-md-4">
                        <div class="card card-blog">
                            <div class="card-image">
                                <a href="<?php the_permalink(); ?>">
                                  <?php the_post_thumbnail('medium', array('class' => 'img img-raised')); ?>
                                </a>
                            </div>
                            <div class="card-body">
                              <?php
                                  $categories_list = get_the_category_list( esc_html__( ', ', 'rensya' ) );
                                  if ( $categories_list ) {
                                    /* translators: 1: list of categories. */
                                    printf( '<h6 class="card-category text-success">' . esc_html__( '%1$s', 'rensya' ) . '</h6>', $categories_list ); // WPCS: XSS OK.
                                  }
                              ?>
                                <h5 class="card-title">
                        <a href="<?php the_permalink(); ?>"><?php echo mb_strimwidth(get_the_title(), 0, 20, '...'); ?></a>
                      </h5>
                                <p class="card-description">
                                    <?php the_excerpt(); ?>
                                    <br>
                                </p>
                                <hr>
                                <div class="card-footer">
                                    <div class="author">
                                        <a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>">
                                          <?php echo get_avatar( get_the_author_meta( 'ID' ), 80, '', '', array('class' => 'avatar img-raised') ); ?>
                                            <span><?php the_author(); ?></span>
                                        </a>
                                    </div>
                                    <div class="stats">
                                        <i class="fa fa-calendar" aria-hidden="true"></i> <?php the_date('d/m/Y'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                      </div>
                      <?php
                    }
                    /* Restore original Post Data */
                    wp_reset_postdata();
                  } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>