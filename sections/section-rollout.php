
<?php global $mwt_options; ?>
<?php if( $mwt_options['enable_rollout'] == 1 ): 
$bg_image = ( isset($mwt_options['rollout_bg_image']['url']) && $mwt_options['rollout_bg_image']['url'] != '' ) ? $mwt_options['rollout_bg_image']['url'] : get_template_directory_uri() . '/assets/img/contact-bg.jpg';
$stage1_title = 'Stage 1';
$stage2_title = 'Stage 2';
$stage3_title = 'Stage 3';
?>
<div id="section-rollout" class="section section-image" style="background-image: url('<?php echo $bg_image; ?>')">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-8">
				<h2 class="title"><?php echo $mwt_options['rollout_title']; ?></h2>
        <h5 class="subtitle"><?php echo $mwt_options['rollout_subtitle']; ?></h5>        
			</div>
      <?php if( isset($mwt_options['logo']['url']) && $mwt_options['logo']['url'] != '' ) : ?>
      <div class="col-sm-12 col-md-4 d-none d-md-block d-lg-block">
				<img src="<?php echo $mwt_options['logo']['url']; ?>" class="img-fluid main-logo">     
			</div>
      <?php endif; ?>
		</div>
	</div>
  <form class="form cf">
    <div class="stepper d-none d-md-block d-lg-block">
      <div class="stepper-inner">
        <div class="connecting-line"></div>
        <div class="container">
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="nav-item text-center">
              <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Step 1" class="nav-link">
              <span class="round-tab"></span>
              </a>
              <h5 class="stage">
                <?php echo $stage1_title; ?>
                <small><?php echo $mwt_options['rollout_state_1']; ?></small>
              </h5>
            </li>
            <li role="presentation" class="nav-item text-center">
              <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Step 2" class="nav-link disabled">
              <span class="round-tab"></span>
              </a>
              <h5 class="stage">
                <?php echo $stage2_title; ?>
                <small><?php echo $mwt_options['rollout_state_2']; ?></small>
              </h5>
            </li>
            <li role="presentation" class="nav-item text-center">
              <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Step 3" class="nav-link disabled">
              <span class="round-tab"></span>
              </a>
              <h5 class="stage">
                <?php echo $stage3_title; ?>
                <small><?php echo $mwt_options['rollout_state_3']; ?></small>
              </h5>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </form>
  <div class="container d-block d-md-none d-lg-none">
    <div class="mobile-stepper">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title"><?php echo $stage1_title; ?></h4>
          <p><?php echo $mwt_options['rollout_state_1']; ?></p>
        </div>
      </div>
      <div class="card">
        <div class="card-body">
          <h4 class="card-title"><?php echo $stage2_title; ?></h4>
          <p><?php echo $mwt_options['rollout_state_2']; ?></p>
        </div>
      </div>
      <div class="card">
        <div class="card-body">
          <h4 class="card-title"><?php echo $stage3_title; ?></h4>
          <p><?php echo $mwt_options['rollout_state_3']; ?></p>
        </div>
      </div>
    </div>
    
    <div class="text-center partner-logo">
      <?php $partner_gallery = explode( ",", $mwt_options['partner_logo'] ); 
      $count = 1;
      foreach( $partner_gallery as $photo_id ) :
      ?>
        <span class="partner-<?php echo $count; ?>">
          <img src="<?php echo wp_get_attachment_url( $photo_id ); ?>" class="img-fluid">
        </span>
      <?php $count++; endforeach; ?>
    </div>
  </div>
</div>

<?php endif; ?>