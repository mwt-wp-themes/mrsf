<?php

class Mwt {
  
  public function __construct() {
    add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
    add_action( 'wp_enqueue_scripts', array( $this, 'dynamic_enqueue_scripts' ) );
    $this->load_dependencies();
    if( is_admin() ) {
      add_action( 'admin_enqueue_scripts', array( $this, 'register_admin_scripts' ) );
    }
  }
  
  public function enqueue_scripts() {
    wp_enqueue_style( 'google-font-style', 'https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700|Poppins:400,700,800', array(), '', '' );
    wp_enqueue_style( 'fancybox-style', get_stylesheet_directory_uri() . '/assets/css/jquery.fancybox.min.css', array(), false, '' );
      wp_enqueue_style( 'animate-style', get_stylesheet_directory_uri() . '/assets/css/animate.min.css', array(), false, '' );
    wp_enqueue_style( 'font-awesome-style', get_stylesheet_directory_uri() . '/assets/css/font-awesome.min.css', array(), '4.7.0', '' );
    wp_enqueue_style( 'nucleo-icons-style', get_stylesheet_directory_uri() . '/assets/css/nucleo-icons.css', array(), false, '' );
    wp_enqueue_style( 'mrsf-style', get_stylesheet_uri() );

    //wp_enqueue_script( 'jquery-js', get_template_directory_uri() . '/assets/js/jquery-3.2.1.js', array(), '', true );
    wp_enqueue_script( 'jquery-ui-js', get_template_directory_uri() . '/assets/js/jquery-ui-1.12.1.custom.min.js', array('jquery'), '', true );
    wp_enqueue_script( 'popper-js', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js', array('jquery'), '', true );
    wp_enqueue_script( 'bootstrap-js', 'https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js', array('jquery'), '', true );
    wp_enqueue_script( 'nouislider-js', get_template_directory_uri() . '/assets/js/nouislider.js', array('jquery'), '', true );
    wp_enqueue_script( 'jquery-fancybox-js', get_template_directory_uri() . '/assets/js/jquery.fancybox.min.js', array('jquery'), '', true );
    wp_enqueue_script( 'jquery-easing-js', get_template_directory_uri() . '/assets/js/jquery.easing.min.js', array('jquery'), '', true );
    wp_enqueue_script( 'jquery-scrolling-nav-js', get_template_directory_uri() . '/assets/js/scrolling-nav.js', array('jquery'), '', true );
    wp_enqueue_script( 'loading-overlay-js', 'https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.6/dist/loadingoverlay.min.js', array('jquery'), '', true );
    wp_enqueue_script( 'paper-kit-js', get_template_directory_uri() . '/assets/js/paper-kit.js', array('jquery'), '', true );
    wp_enqueue_script( 'app-scripts' );
    wp_register_script( 'app-scripts', get_stylesheet_directory_uri() . '/assets/js/app.js', array('jquery'), '', true);
    wp_localize_script( 'app-scripts', 'mrsf_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ), 'wp_nonce' => wp_create_nonce( 'mwt-nonce' ) ) );
    wp_enqueue_script( 'app-scripts' );
    
    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
      wp_enqueue_script( 'comment-reply' );
    }
  }
  
  public function dynamic_enqueue_scripts() {
    global $mwt_options;
    $custom_js = '';
    $custom_css = '';
    if( is_front_page() ) {
      wp_enqueue_style( 'owl-carousel-style', get_stylesheet_directory_uri() . '/assets/plugins/owl-carousel/assets/owl.carousel.min.css', array(), false, '' );
      wp_enqueue_style( 'owl-theme-style', get_stylesheet_directory_uri() . '/assets/plugins/owl-carousel/assets/owl.theme.default.min.css', array(), false, '' );
      wp_enqueue_style( 'select2-style', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css', array(), false, '' );
      
      wp_enqueue_style(
        'custom-style',
        get_template_directory_uri() . '/assets/css/custom.css'
      );
      if( !empty( $mwt_options['info_image_size']['width'] ) ) {
        $custom_css = "
          .info-image img {
                  width: " . $mwt_options['info_image_size']['width'] . ";
                  height: " . $mwt_options['info_image_size']['height'] . ";
                  object-fit: cover;
          }"; 
        }
      wp_add_inline_style( 'custom-style', trim($custom_css) );
      
      wp_enqueue_script( 'owl-carousel', get_template_directory_uri() . '/assets/plugins/owl-carousel/owl.carousel.min.js', array('jquery'), '', true );
      wp_enqueue_script( 'select2-js', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js', array('jquery'), '', true );
      wp_enqueue_script( 'google-chart-js', 'https://www.gstatic.com/charts/loader.js', array('jquery'), '', true );
      $hero_type = $mwt_options['hero_type'];
      if( $hero_type == 'video' ) {
//         $custom_js .= '
//             jQuery(\'button[data-toggle="video"]\').click(function(){
//                 var id_video = jQuery(this).data(\'video\');
//                 var video = jQuery(\'#\' + id_video).get(0);

//                 var parent = jQuery(this).parent(\'div\').parent(\'div\');

//                 if(video.paused){
//                     video.play();
//                     jQuery(this).html(\'<i class="fa fa-pause"></i> Pause Video\');
//                     parent.addClass(\'state-play\');
//                 } else {
//                     video.pause();
//                     jQuery(this).html(\'<i class="fa fa-play"></i> Play Video\');
//                     parent.removeClass(\'state-play\');
//                 }
//             });
//         ';
      }
      $custom_js .= 'jQuery(document).ready(function($){';
      $custom_js .= '
            //$(".select2").select2();
            $.mrsfInitStatistikChart();
            var form = $("#deklarasi-form");
            $(form).submit(function(e) {
              e.preventDefault();
              $.mrsfDeklarasiAction(form);
            })
        '; 
      $custom_js .= '});';
    }
    wp_enqueue_script( 'app-scripts', get_template_directory_uri() . '/assets/js/app.js', array('app-scripts') );
    wp_add_inline_script( 'app-scripts', trim($custom_js) );
  }
  
  private function load_dependencies() {
    if( session_id() == '' ) session_start();
    require get_template_directory() . '/inc/admin/admin-init.php';
    require get_template_directory() . '/inc/function/mwt-dukungan-post-type.php';
    require get_template_directory() . '/inc/function/mwt-faq-post-type.php';
    require get_template_directory() . '/inc/function/mwt-menu.php';
    require get_template_directory() . '/inc/bs4navwalker.php';
    require get_template_directory() . '/inc/function/mwt-core.php';
    require get_template_directory() . '/inc/function/mwt-ajax.php';
    
    require get_template_directory() . '/inc/custom-header.php';
    require get_template_directory() . '/inc/template-tags.php';
    require get_template_directory() . '/inc/template-functions.php';
    require get_template_directory() . '/inc/customizer.php';
    if( is_admin() ) {
      require get_template_directory() . '/inc/tgm-activation/config.php';
      require get_template_directory() . '/inc/function/mwt-admin.php';
    }
    if ( defined( 'JETPACK__VERSION' ) ) {
      require get_template_directory() . '/inc/jetpack.php';
    }
    if ( class_exists( 'WooCommerce' ) ) {
      require get_template_directory() . '/inc/woocommerce.php';
    }
  }
  
  public function register_admin_scripts( $hook ) {
    if (  !in_array( $hook, ['toplevel_page_data_dukungan' ] ) ) {
        return;
    }
    wp_enqueue_style( 'admin_pure_css', 'https://unpkg.com/purecss@1.0.0/build/pure-min.css' );
    wp_enqueue_style( 'admin_datatable_css', get_template_directory_uri() . '/assets/plugins/DataTables/datatables.min.css' );
    wp_enqueue_script( 'admin_datatable_script', get_template_directory_uri() . '/assets/plugins/DataTables/datatables.min.js', array('jquery') );
  }
  
  public static function get_field($field_name = '', $post_id = false) {
    if( !function_exists('get_field') ) {
      return '';
    }
    $post_id = ( !$post_id ) ? get_the_ID() : $post_id;
    return get_field($field_name, $post_id);
  }
  
  public static function update_field($field_name, $field_value = '', $post_id = false) {
    if( !function_exists('update_field') ) {
      return;
    }
    $post_id = ( !$post_id ) ? get_the_ID() : $post_id;
    return update_field($field_name, $field_value, $post_id);
  }
  
}

$mwt = new Mwt();