<?php

Redux::setSection( $opt_name, array(
    'title'      => __( 'Socials', 'rensya' ),
    'desc'       => __( 'Social Media', 'rensya' ),
    'id'         => 'mwt-socials-options',
    'icon'       => 'el el-comment-alt',
    'fields'     => array(
        array(
            'id'       => 'social-facebook',
            'type'     => 'text',
            'title'    => __( 'Facebook URL', 'rensya' ),
            'default'  => '#',
        ),
        array(
            'id'       => 'social-twitter',
            'type'     => 'text',
            'title'    => __( 'Twitter URL', 'rensya' ),
            'default'  => '#',
        ),
        array(
            'id'       => 'social-google-plus',
            'type'     => 'text',
            'title'    => __( 'Google Plus URL', 'rensya' ),
            'default'  => '#',
        ),
        array(
            'id'       => 'social-instagram',
            'type'     => 'text',
            'title'    => __( 'Instagram URL', 'rensya' ),
            'default'  => '#',
        ),
        array(
            'id'       => 'social-youtube',
            'type'     => 'text',
            'title'    => __( 'Youtube URL', 'rensya' ),
            'default'  => '#',
        ),
        array(
            'id'       => 'social-linkedin',
            'type'     => 'text',
            'title'    => __( 'LinkedIn URL', 'rensya' ),
            'default'  => '#',
        ),
        array(
            'id'       => 'social-pinterest',
            'type'     => 'text',
            'title'    => __( 'Tumblr URL', 'rensya' ),
            'default'  => '#',
        ),
        array(
            'id'       => 'social-tumblr',
            'type'     => 'text',
            'title'    => __( 'Tumblr URL', 'rensya' ),
            'default'  => '#',
        ),
        array(
            'id'       => 'social-rss',
            'type'     => 'text',
            'title'    => __( 'RSS URL', 'rensya' ),
            'default'  => '#',
        ),
    )
) );