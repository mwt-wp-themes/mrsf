<?php

function mwt_page_header() {
  global $mwt_options;
	$custom_page_tempates = array(
		'template-contact-us.php',
	);
  $header_bg_image = ( isset($mwt_options['header_bg_image']['url']) && $mwt_options['header_bg_image']['url'] != '' ) ? $mwt_options['header_bg_image']['url'] : get_header_image();
  if( !is_front_page() && !in_array( get_page_template_slug(), $custom_page_tempates ) ) {
    ?>
		<header class="page-header page-header-xs" style="background-image: url('<?php echo $header_bg_image; ?>');">
      <div class="filter"></div>
			<div class="content">
				<div class="container text-center">
            <?php if ( is_home() && ! is_front_page() ) : ?>
              <h2 class="page-title screen-reader-text"><?php single_post_title(); ?></h2>
            <?php elseif( is_front_page() ) : ?>
            <?php mwt_front_page_slider(); ?>
						<?php elseif ( is_page() ) : ?>
							<h1 class="page-title"><?php echo get_the_title(); ?></h1>
						<?php elseif ( is_singular() ) : ?>
							<?php single_post_title( '<h1 class="page-title">', '</h1>' ); ?>
						<?php elseif ( is_archive() ) : ?>
							<?php if( get_post_type() != 'post' ): ?>
							<?php post_type_archive_title( '<h1 class="page-title">', '</h1>' ); ?>
							<?php endif; ?>
						<?php endif; ?>
				</div>
			</div>
    </header>
    <?php
  }
}

function mwt_enqueue_client_carousel() {
  $scripts = 'jQuery(document).ready(function() {
			jQuery("#home-client-carousel").owlCarousel({
					loop:true,
					margin:20,
					nav:false,
					dots:true,
					lazyLoad:true,
					autoplay:true,
					autoplayTimeout:2000,
					autoplayHoverPause:false,
					animateIn:true,
					animateOut:true,
					responsive:{
							0:{
									items:2
							},
							600:{
									items:4
							},
							1000:{
									items:5
							}
					}
			});
		});';
   wp_add_inline_script( 'rensya-js', $scripts );
}
add_action( 'wp_enqueue_scripts', 'mwt_enqueue_client_carousel' );

function mwt_front_page_slider() {
	global $mwt_options; 
	if( is_front_page() && $mwt_options['enable_slider'] == 1 ) { 
			$home_slides = $mwt_options['home_slides']; 
			$slides_count = count($home_slides);
			if ( $slides_count > 0 ) : ?>
			<div class="page-carousel">
					<div class="filter"></div>
					<div id="carouselIndicators" class="carousel slide" data-ride="carousel">
              <?php if( $slides_count > 1 ) : ?>
							<ol class="carousel-indicators">
								<?php for( $i = 0; $i < $slides_count; $i++ ) {
									$active = ( $i == 0 ) ? 'active' : '';
									echo '<li data-target="#carouselIndicators" data-slide-to="'.$i.'" class="'.$active.'"></li>';
								} ?>
							</ol>
              <?php endif; ?>
							<div class="carousel-inner" role="listbox">
									<?php for( $i = 0; $i < $slides_count; $i++ ): ?>
									<div class="carousel-item<?php echo ( $i == 0 ) ? " active" : "" ; ?>">
											<div class="page-header wow fadeIn" style="background-image: url('<?php echo $home_slides[$i]['image']; ?>');" data-wow-duration="2s">
													<div class="filter"></div>
													<div class="content-center">
															<div class="container">
																	<div class="row">
																		<?php
																		if ( $mwt_options['slide_alignment'] == 'left' ) {
																			$column_class = "col-md-6 text-left";
																		} elseif ( $mwt_options['slide_alignment'] == 'right' ) {
																			$column_class = "col-md-7 ml-auto text-right";
																		} else {
																			$column_class = "col-md-8 ml-auto mr-auto text-center";
																		}
																		?>
																			<div class="<?php echo $column_class; ?>">
																					<?php if( $home_slides[$i]['title'] != '' ): ?>
																					<h1 class="title wow fadeInDown" data-wow-duration="1s"><?php echo $home_slides[$i]['title']; ?></h1>
																					<?php endif; ?>
																					<?php if( $home_slides[$i]['description'] != '' ): ?>
																					<h5 class="wow fadeInUp" data-wow-duration="1s"><?php echo $home_slides[$i]['description']; ?></h5>
																					<br />
																					<?php endif; ?>
																					<?php if( $home_slides[$i]['url'] != '' ): ?>
																					<div class="buttons wow fadeIn" data-wow-delay="1s">
																							<a href="<?php echo $home_slides[$i]['url']; ?>" class="btn btn-primary btn-round  btn-lg">
																									<?php echo $mwt_options['slide_button_text']; ?>
																							</a>
																					</div>
																					<?php endif; ?>
																			</div>
																	</div>
															</div>
													</div>
											</div>
									</div>
									<?php endfor; ?>
							</div>
              <?php if( $slides_count > 1 ) : ?>
							<a class="left carousel-control carousel-control-prev" href="#carouselIndicators" role="button" data-slide="prev">
									<span class="fa fa-angle-left"></span>
									<span class="sr-only"><?php _e('Previous', 'rensya'); ?></span>
							</a>
							<a class="right carousel-control carousel-control-next" href="#carouselIndicators" role="button" data-slide="next">
									<span class="fa fa-angle-right"></span>
									<span class="sr-only"><?php _e('Next', 'rensya'); ?></span>
							</a>
              <?php endif; ?>
					</div>
			</div>
	<?php endif; }
}

function mwt_enqueue_typography() {
	global $mwt_options;
	$typography = $mwt_options['typography'];
	$font_family = '"'.$typography['font-family'].'"';
	if ( isset($typography['font-backup']) && $typography['font-backup'] != '' ) {
		$font_family .= ', '.$typography['font-backup'];
	}
	$css = "
		body {
			color: {$typography['color']};
			font-size: {$typography['font-size']};
			font-weight: {$typography['font-weight']};
			font-family: {$font_family};
		}
		h1, .h1, h2, .h2, h3, .h3, h4, .h4, h5, .h5, h6, .h6, p, .navbar, .brand, a, .td-name, td, button, input, select, textarea {
			font-family: {$font_family};
			font-weight: {$typography['font-weight']};
		}
		.title,
		.card-title,
		.info-title,
		.footer-brand,
		.footer-big h5,
		.footer-big h4,
		.media .media-heading {
			font-family: {$font_family};
		}";
	wp_add_inline_style( 'paper-kit-style', $css );
}
add_action( 'wp_enqueue_scripts', 'mwt_enqueue_typography' );

function mwt_navigation() {
	global $mwt_options;
	if( get_page_template_slug() == 'template-contact-us.php' ) {
		$nav_class = "navbar navbar-expand-lg fixed-top bg-primary";
		$color_on_scroll = "";
	} else {
		$nav_class = "navbar navbar-expand-md fixed-top navbar-transparent";
		$color_on_scroll = "color-on-scroll=\"300\"";
	}
	?>
			<nav class="<?php echo $nav_class; ?>" <?php echo $color_on_scroll; ?>>
					<div class="container">
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="navbar-brand">
								<?php $custom_logo_id = get_theme_mod( 'custom_logo' );
								$logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
								if ( has_custom_logo() ) {
												echo '<img src="'. esc_url( $logo[0] ) .'">';
								} else {
												echo get_bloginfo( 'name' );
								} ?>						
							</a>
							<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
									<span class="navbar-toggler-bar"></span>
									<span class="navbar-toggler-bar"></span>
									<span class="navbar-toggler-bar"></span>
							</button>

							<div class="collapse navbar-collapse" id="navbarSupportedContent">
								<?php mwt_top_nav(); ?>
							</div>
					</div>
			</nav><?php
}
function mwt_pagination( $args = array() ) {
    
    $defaults = array(
        'range'           => 4,
        'custom_query'    => FALSE,
        'previous_string' => __( 'Previous', 'rensya' ),
        'next_string'     => __( 'Next', 'rensya' ),
        'before_output'   => '<div class="post-nav"><ul class="pagination justify-content-center">',
        'after_output'    => '</ul></div>'
    );
    
    $args = wp_parse_args( 
        $args, 
        apply_filters( 'wp_bootstrap_pagination_defaults', $defaults )
    );
    
    $args['range'] = (int) $args['range'] - 1;
    if ( !$args['custom_query'] )
        $args['custom_query'] = @$GLOBALS['wp_query'];
    $count = (int) $args['custom_query']->max_num_pages;
    $page  = intval( get_query_var( 'paged' ) );
    $ceil  = ceil( $args['range'] / 2 );
    
    if ( $count <= 1 )
        return FALSE;
    
    if ( !$page )
        $page = 1;
    
    if ( $count > $args['range'] ) {
        if ( $page <= $args['range'] ) {
            $min = 1;
            $max = $args['range'] + 1;
        } elseif ( $page >= ($count - $ceil) ) {
            $min = $count - $args['range'];
            $max = $count;
        } elseif ( $page >= $args['range'] && $page < ($count - $ceil) ) {
            $min = $page - $ceil;
            $max = $page + $ceil;
        }
    } else {
        $min = 1;
        $max = $count;
    }
    
    $echo = '';
    $previous = intval($page) - 1;
    $previous = esc_attr( get_pagenum_link($previous) );
    
    $firstpage = esc_attr( get_pagenum_link(1) );
    if ( $firstpage && (1 != $page) )
        $echo .= '<li class="previous"><a href="' . $firstpage . '">' . __( 'First', 'rensya' ) . '</a></li>';
    if ( $previous && (1 != $page) )
        $echo .= '<li><a href="' . $previous . '" title="' . __( 'previous', 'rensya') . '">' . $args['previous_string'] . '</a></li>';
    
    if ( !empty($min) && !empty($max) ) {
        for( $i = $min; $i <= $max; $i++ ) {
            if ($page == $i) {
                $echo .= '<li class="active"><span class="active">' . str_pad( (int)$i, 2, '0', STR_PAD_LEFT ) . '</span></li>';
            } else {
                $echo .= sprintf( '<li><a href="%s">%002d</a></li>', esc_attr( get_pagenum_link($i) ), $i );
            }
        }
    }
    
    $next = intval($page) + 1;
    $next = esc_attr( get_pagenum_link($next) );
    if ($next && ($count != $page) )
        $echo .= '<li><a href="' . $next . '" title="' . __( 'next', 'rensya') . '">' . $args['next_string'] . '</a></li>';
    
    $lastpage = esc_attr( get_pagenum_link($count) );
    if ( $lastpage ) {
        $echo .= '<li class="next"><a href="' . $lastpage . '">' . __( 'Last', 'rensya' ) . '</a></li>';
    }
    if ( isset($echo) )
        echo $args['before_output'] . $echo . $args['after_output'];
}

add_filter( 'get_custom_logo', 'mwt_change_logo_class' );
function mwt_change_logo_class( $html ) {
    $html = str_replace( 'custom-logo-link', 'navbar-brand', $html );
    return $html;
}

// hide admin bar
add_filter('show_admin_bar', '__return_false');

function mwt_excerpt_length( $length ) {
    return 20;
}
add_filter( 'excerpt_length', 'mwt_excerpt_length', 999 );

function mwt_comments( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment; 
	?>
	<?php if ( $comment->comment_approved == '1' ): ?>
	<li class="list-group-item media">
		<div class="media-left">
			<?php echo get_avatar( $comment ); ?>
		</div>
		<div class="media-body">
			<h4 class="media-heading">
				<?php comment_author_link() ?>
			</h4>
			<time>
				<a href="#comment-<?php comment_ID() ?>" pubdate>
					<?php comment_date() ?> at <?php comment_time() ?>
				</a>
			</time>
			<?php comment_text() ?>
		</div>
	<?php endif;
}

function mwt_related_post() {
    global $post;
    $tag_arr = '';
    $tags = wp_get_post_tags( $post->ID );
    if($tags) {
        foreach( $tags as $tag ) {
            $tag_arr .= $tag->slug . ',';
        }
        $args = array(
            'tag' => $tag_arr,
            'numberposts' => 3, /* you can change this to show more */
            'post__not_in' => array($post->ID),
            'posts_per_page'=> 5, // Number of related posts to display.
        );
    } else {
        $args = array(
            'numberposts' => 3, /* you can change this to show more */
            'post__not_in' => array($post->ID),
            'posts_per_page'=> 5, // Number of related posts to display.
        );
    }
    $related_posts = get_posts( $args );
    if($related_posts) { ?>

        <div class="related-articles">
            <h3 class="title">Related articles</h3>
            <legend></legend>
            <div class="container">
                <div class="row">
									<?php foreach ( $related_posts as $post ) : setup_postdata( $post ); ?>
										<div class="col-md-4 ml-auto mr-auto">
                        <a href="<?php the_permalink(); ?>">
													<?php the_post_thumbnail('thumbnail', array('class' => 'img-rounded img-responsive')); ?>
												</a>
                        <p class="blog-title"><?php the_title(); ?></p>
                    </div>
									<?php endforeach; ?>
                </div>
            </div>
        </div><?php
    }
    wp_reset_postdata();
}

function mwt_front_page_header() {
  global $mwt_options;
  $hero_type = $mwt_options['hero_type'];
  $alignment = $mwt_options['banner_alignment'];
  $html = '<div class="hero-' . $hero_type . ' wow fadeIn" data-wow-duration="2s">';
  if( $hero_type == 'video' ) {
    $background_image = ( isset($mwt_options['header_bg_image']['url']) && $mwt_options['header_bg_image']['url'] != '' ) ? $mwt_options['header_bg_image']['url'] : get_template_directory_uri() . '/assets/img/contact-bg.jpg';
//     $html .= '<div class="header header-video">';
//     //$html .= '<div class="video-image visible-xs visible-sm" style="background-image: url(\'' . $background_image . '\')"></div>';
//     $html .= '
//         <video id="video-source" autoplay muted loop>
//           <source src="' . $mwt_options['video_url'] . '" type="video/mp4">
//           Video not supported
//         </video>
//         <div class="filter filter-danger"></div>
//         <div class="container upper-container text-center">
//     ';
//     $html .= '<div class="row">';
//     $html .= '<div class="col-md-12 text-'.$alignment.'">';
    
//     if( isset($mwt_options['logo']['url']) && $mwt_options['logo']['url'] != '' ) {
//       $html .= '<img src="' . $mwt_options['logo']['url'] . '" class="logo img-fluid">';
//     }
    
//     if( !empty( $mwt_options['banner_subtitle'] ) ) {
//       $html .= '<h4 class="subtitle">' . $mwt_options['banner_subtitle'] . '</h4>'; 
//     }
//     if( !empty( $mwt_options['banner_title'] ) ) {
//       $html .= '<h1>' . $mwt_options['banner_title'] . '</h1>'; 
//     }
//     if( !empty( $mwt_options['banner_description'] ) ) {
//       $html .= '<p class="description">' . $mwt_options['banner_description'] . '</p>'; 
//     }
//     $html .= '</div>';
//     $html .= '</div>';
//     $html .= '<div class="row">';
//     $html .= '<div class="col-md-12 text-center partner-logo">';
    
//     $partner_gallery = explode( ",", $mwt_options['partner_logo'] ); 
//     $count = 1;
//     foreach( $partner_gallery as $photo_id ) {
//       $html .= '<span class="partner-' . $count . '">';
//       $html .= '<img src="' . wp_get_attachment_url( $photo_id ) . '" class="img-fluid">';
//       $html .= '</span>';
//     $count++; };
    
//     $html .= '</div></div>';
//     $html .= '</div></div>';
    

    $background_image = ( isset($mwt_options['banner_bg_image']['url']) && $mwt_options['banner_bg_image']['url'] != '' ) ? $mwt_options['banner_bg_image']['url'] : get_template_directory_uri() . '/assets/img/contact-bg.jpg';
    $html .= '<header class="header page-header header-video';
    $html .= ( $mwt_options['banner_size'] != 'large' ) ? ' page-header-' . $mwt_options['banner_size'] : '';
    $html .= '">';
    $html .= '
        <video id="video-source" autoplay muted loop>
          <source src="' . $mwt_options['video_url'] . '" type="video/mp4">
          Video not supported
        </video>
    ';
    $html .= '<div class="filter filter-warning"></div>';
    $html .= '<div class="container text-' . $alignment . '">';
    $html .= '<div class="row">';
    $html .= '<div class="col-md-12 text-'.$alignment.'">';
    
    if( isset($mwt_options['logo']['url']) && $mwt_options['logo']['url'] != '' ) {
      $html .= '<img src="' . $mwt_options['logo']['url'] . '" class="logo img-fluid wow rollIn">';
    }
    
    if( !empty( $mwt_options['banner_subtitle'] ) ) {
      $html .= '<h4 class="subtitle">' . $mwt_options['banner_subtitle'] . '</h4>'; 
    }
    if( !empty( $mwt_options['banner_title'] ) ) {
      $html .= '<h1>' . $mwt_options['banner_title'] . '</h1>'; 
    }
    if( !empty( $mwt_options['banner_description'] ) ) {
      $html .= '<p class="description">' . $mwt_options['banner_description'] . '</p>'; 
    }
    $html .= '</div>';
    $html .= '</div>';
    $html .= '<div class="row">';
    $html .= '<div class="col-md-12 text-center partner-logo">';
    
    $partner_gallery = explode( ",", $mwt_options['partner_logo'] ); 
    $count = 1;
    foreach( $partner_gallery as $photo_id ) {
      $html .= '<span class="partner-' . $count . ' wow ';
      $html .= ( $count == 1 ) ? 'fadeInLeft' : 'fadeInRight';
      $html .= '">';
      $html .= '<img src="' . wp_get_attachment_url( $photo_id ) . '" class="img-fluid">';
      $html .= '</span>';
    $count++; };
    
    $html .= '</div></div>';
    $html .= '</div></header>';
    
    
  } elseif( $hero_type == 'banner_olg' ) {
//     $background_image = ( isset($mwt_options['banner_bg_image']['url']) && $mwt_options['banner_bg_image']['url'] != '' ) ? $mwt_options['banner_bg_image']['url'] : get_template_directory_uri() . '/assets/img/contact-bg.jpg';
//     $html .= '<header class="header page-header';
//     $html .= ( $mwt_options['banner_size'] != 'large' ) ? ' page-header-' . $mwt_options['banner_size'] : '';
//     $html .= '" style="background-image: url(\'' . $background_image . '\');">';
//     $html .= '<div class="filter filter-warning"></div>';
//     $html .= '<div class="container text-' . $alignment . '">';
//     $html .= '<div class="row">';
//     if( $alignment == 'left' ) {
//       $html .= '<div class="col-sm-12 col-md-7 left">';
//     }
//     if( !empty( $mwt_options['banner_subtitle'] ) ) {
//       $html .= '<h4 class="subtitle">' . $mwt_options['banner_subtitle'] . '</h4>'; 
//     }
//     if( !empty( $mwt_options['banner_title'] ) ) {
//       $html .= '<h1>' . $mwt_options['banner_title'] . '</h1>'; 
//     }
//     if( !empty( $mwt_options['banner_description'] ) ) {
//       $html .= '<p class="description">' . $mwt_options['banner_description'] . '</p>'; 
//     }
//     if( $alignment == 'left' ) {
//       $html .= '</div><div class="col-sm-12 col-md-5 right">';
//     }
    
//     if( isset($mwt_options['logo']['url']) && $mwt_options['logo']['url'] != '' ) {
//       $html .= '<img src="' . $mwt_options['logo']['url'] . '" class="logo img-responsive">';
//     }
    
//     if( $alignment == 'left' ) {
//       $html .= '</div>';
//     }
//     $html .= '</div>';
//     $html .= '</div></header>';
  } else {
    $background_image = ( isset($mwt_options['banner_bg_image']['url']) && $mwt_options['banner_bg_image']['url'] != '' ) ? $mwt_options['banner_bg_image']['url'] : get_template_directory_uri() . '/assets/img/contact-bg.jpg';
    $html .= '<header class="header page-header';
    $html .= ( $mwt_options['banner_size'] != 'large' ) ? ' page-header-' . $mwt_options['banner_size'] : '';
    $html .= '" style="background-image: url(\'' . $background_image . '\');">';
    $html .= '<div class="filter filter-warning"></div>';
    $html .= '<div class="container text-' . $alignment . '">';
    $html .= '<div class="row">';
    $html .= '<div class="col-md-12 text-'.$alignment.'">';
    
    if( isset($mwt_options['logo']['url']) && $mwt_options['logo']['url'] != '' ) {
      $html .= '<img src="' . $mwt_options['logo']['url'] . '" class="logo img-fluid wow rollIn">';
    }
    
    if( !empty( $mwt_options['banner_subtitle'] ) ) {
      $html .= '<h4 class="subtitle">' . $mwt_options['banner_subtitle'] . '</h4>'; 
    }
    if( !empty( $mwt_options['banner_title'] ) ) {
      $html .= '<h1>' . $mwt_options['banner_title'] . '</h1>'; 
    }
    if( !empty( $mwt_options['banner_description'] ) ) {
      $html .= '<p class="description">' . $mwt_options['banner_description'] . '</p>'; 
    }
    $html .= '</div>';
    $html .= '</div>';
    $html .= '<div class="row">';
    $html .= '<div class="col-md-12 text-center partner-logo">';
    
    $partner_gallery = explode( ",", $mwt_options['partner_logo'] ); 
    $count = 1;
    foreach( $partner_gallery as $photo_id ) {
      $html .= '<span class="partner-' . $count . ' wow ';
      $html .= ( $count == 1 ) ? 'fadeInLeft' : 'fadeInRight';
      $html .= '">';
      $html .= '<img src="' . wp_get_attachment_url( $photo_id ) . '" class="img-fluid">';
      $html .= '</span>';
    $count++; };
    
    $html .= '</div></div>';
    $html .= '</div></header>';
  }
  $html .= '</div>';
  echo $html;
}