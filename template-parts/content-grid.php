<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package rensya
 */
global $mwt_options;
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
		<div class="card card-blog">
				<div class="card-image">
						<a href="<?php the_permalink(); ?>">
							<?php the_post_thumbnail('medium', array('class' => 'img img-raised')); ?>
						</a>
				</div>
				<div class="card-body">
					<?php
							$categories_list = get_the_category_list( esc_html__( ', ', 'rensya' ) );
							if ( $categories_list ) {
								/* translators: 1: list of categories. */
								printf( '<h6 class="card-category text-success">' . esc_html__( '%1$s', 'rensya' ) . '</h6>', $categories_list ); // WPCS: XSS OK.
							}
					?>
						<h5 class="card-title">
		<a href="<?php the_permalink(); ?>"><?php echo mb_strimwidth(get_the_title(), 0, 20, '...'); ?></a>
	</h5>
						<p class="card-description">
								<?php the_excerpt(); ?>
								<br>
						</p>
						<hr>
						<div class="card-footer">
								<div class="author">
										<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>">
											<?php echo get_avatar( get_the_author_meta( 'ID' ), 80, '', '', array('class' => 'avatar img-raised') ); ?>
												<span><?php the_author(); ?></span>
										</a>
								</div>
								<div class="stats">
										<i class="fa fa-calendar" aria-hidden="true"></i> <?php the_date('d/m/Y'); ?>
								</div>
						</div>
				</div>
		</div>
	
</article><!-- #post-<?php the_ID(); ?> -->
