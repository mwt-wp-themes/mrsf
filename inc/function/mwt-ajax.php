<?php

add_action( 'wp_ajax_mrsf_submit_deklarasi', 'mrsf_submit_deklarasi' );
add_action( 'wp_ajax_nopriv_mrsf_submit_deklarasi', 'mrsf_submit_deklarasi' );
function mrsf_submit_deklarasi() {
  global $mwt_options;
  $result = [];
  $nonce = $_POST['wp_nonce'];
  if ( ! wp_verify_nonce( $nonce, 'mwt-nonce' ) ) {
      // This nonce is not valid.
      die( 'Security check' ); 
  } else {
      $result = $_POST;
  }
  $nama = $_POST['nama'];
  $usia = $_POST['usia'];
  $email = $_POST['email'];
  $no_hp = $_POST['no_hp'];
  $args = array(
    'post_title'    => $nama,
    'post_status'   => 'publish',
    'post_author'   => 1,
    'post_type'     => 'dukungan'
  );
  $post_id = wp_insert_post( $args );
  wp_set_object_terms( $post_id, intval( $_POST['polres'] ), 'polres' );
  Mwt::update_field( 'nama', $nama, $post_id );
  Mwt::update_field( 'usia', $usia, $post_id );
  Mwt::update_field( 'nomor_hp', $no_hp, $post_id );
  Mwt::update_field( 'email', $email, $post_id );
  
  $result['id'] = $post_id;
  $result['msg'] = $mwt_options['form_success_message'];
	echo json_encode( $result );
	wp_die(); 
  
}

add_action( 'wp_ajax_mrsf_get_data_statistik_dukungan', 'mrsf_get_data_statistik_dukungan' );
add_action( 'wp_ajax_nopriv_mrsf_get_data_statistik_dukungan', 'mrsf_get_data_statistik_dukungan' );
function mrsf_get_data_statistik_dukungan() {
  global $mwt;
  $result = [];
  $data_chart_polres = array(
    ['Dukungan Polres', 'Jumlah']
  );
  $data_chart_usia = array(
    ['Usia Millenial', 'Jumlah']
  );
  // WP_Query arguments
  $args = array(
    'post_type'              => array( 'dukungan' ),
    'post_status'            => array( 'publish' ),
    'posts_per_page'         => -1,
  );

  // The User Query
  $posts = get_posts( $args );

  $polres_counter = array();
  $usia_counter = array();
  foreach ( $posts as $post ) {
    
    $post_id = $post->ID;
    $polres = get_the_terms( $post_id, 'polres' );
    $polres = !empty( $polres[0] ) ? $polres[0] : null;
    $usia = Mwt::get_field( 'usia', $post_id );
    $usia = ( !empty( $usia ) ) ? $usia . " tahun" : 'Tidak diisi';

    if( isset( $polres_counter[$polres->name] ) ) {
      $polres_counter[$polres->name]++;
    } else {
      $polres_counter[$polres->name] = intval(1);
    }
    
    if( isset( $usia_counter[$usia] ) ) {
      $usia_counter[$usia]++;
    } else {
      $usia_counter[$usia] = intval(1);
    }
    
  }
  
  foreach( $polres_counter as $name => $value ) {
    $data_chart_polres[] = [$name, $value];
  }
  foreach( $usia_counter as $name => $value ) {
    $name = (string) $name;
    $data_chart_usia[] = [$name, $value];
  }
  $result = array(
    'polres' => $data_chart_polres,
    'usia'   => $data_chart_usia,
  );
  
	echo json_encode( [ 'data' => $result ] );
	wp_die(); 
  
}

add_action( 'wp_ajax_mrsf_get_data_dukungan', 'mrsf_get_data_dukungan' );
add_action( 'wp_ajax_nopriv_mrsf_get_data_dukungan', 'mrsf_get_data_dukungan' );
function mrsf_get_data_dukungan() {
  global $mwt;
  $result = [];
  
  // WP_Query arguments
  $args = array(
    'post_type'              => array( 'dukungan' ),
    'post_status'            => array( 'publish' ),
    'posts_per_page'         => -1,
  );

  // The User Query
  $posts = get_posts( $args );

  foreach ( $posts as $post ) {
    
    $post_id = $post->ID;
    $polres = get_the_terms( $post_id, 'polres' );
    $polres = !empty( $polres[0] ) ? $polres[0] : null;
    
    $result[] = array(
        'id'          => $post->ID,
        'tanggal'     => $post->post_date,
        'nama'        => Mwt::get_field( 'nama', $post_id ),
        'usia'        => Mwt::get_field( 'usia', $post_id ),
        'email'       => Mwt::get_field( 'email', $post_id ),
        'hp'          => Mwt::get_field( 'nomor_hp', $post_id ),
        'polres'      => $polres->name,
    );

  }
  
	echo json_encode( [ 'data' => $result ] );
	wp_die(); 
  
}

add_action( 'wp_ajax_mrsf_hapus_data_dukungan', 'mrsf_hapus_data_dukungan' );
add_action( 'wp_ajax_nopriv_mrsf_hapus_data_dukungan', 'mrsf_hapus_data_dukungan' );
function mrsf_hapus_data_dukungan() {
	$post_id = $_POST['id'];
  wp_delete_post( $post_id, true );
	wp_die();
}

