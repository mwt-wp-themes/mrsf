<?php global $mwt_options; ?>
<?php if( $mwt_options['enable_info'] == 1 ): 
$bg_image = ( isset($mwt_options['info_bg_image']['url']) && $mwt_options['info_bg_image']['url'] != '' ) ? $mwt_options['info_bg_image']['url'] : get_template_directory_uri() . '/assets/img/contact-bg.jpg';

$info_image_1 = ( isset($mwt_options['info_image_1']['url']) && $mwt_options['info_image_1']['url'] != '' ) ? $mwt_options['info_image_1']['url'] : 'https://via.placeholder.com/400x300';
$info_image_2 = ( isset($mwt_options['info_image_2']['url']) && $mwt_options['info_image_2']['url'] != '' ) ? $mwt_options['info_image_2']['url'] : 'https://via.placeholder.com/400x300';
$info_image_3 = ( isset($mwt_options['info_image_3']['url']) && $mwt_options['info_image_3']['url'] != '' ) ? $mwt_options['info_image_3']['url'] : 'https://via.placeholder.com/400x300';
?>
<div id="section-info" class="section section-info-about section-image" style="background-image: url('<?php echo $bg_image; ?>')">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-9 col-md-8">
				<h2 class="title wow fadeInUp"><?php echo $mwt_options['info_title']; ?></h2>
			</div>
      <?php if( isset($mwt_options['logo']['url']) && $mwt_options['logo']['url'] != '' ) : ?>
      <div class="col-3 col-md-4">
				<img src="<?php echo $mwt_options['logo']['url']; ?>" class="img-fluid main-logo">     
			</div>
      <?php endif; ?>
		</div>
	</div>
  
	<div class="container">
    <?php if( $mwt_options['info_image_show'] == 1 ): ?>
		<div class="row">
			<div class="col-md-10 ml-auto mr-auto">
        <div class="row">
          <div class="col-sm-12 col-md-4">
            <div class="info-image wow fadeIn" data-wow-duration="2s">
              <img class="img-rounded img-responsive" src="<?php echo $info_image_1; ?>">
            </div>
          </div>
          <div class="col-sm-12 col-md-4">
            <div class="info-image wow fadeIn" data-wow-duration="2s">
              <img class="img-rounded img-responsive" src="<?php echo $info_image_2; ?>">
            </div>
          </div>
          <div class="col-sm-12 col-md-4">
            <div class="info-image wow fadeIn" data-wow-duration="2s">
              <img class="img-rounded img-responsive" src="<?php echo $info_image_3; ?>">
            </div>
          </div>
        </div>
			</div>
		</div>
    <?php endif; ?>
		<div class="row">
			<div class="col-md-10 ml-auto mr-auto text-center">
        <div class="description">
          <?php echo $mwt_options['info_description']; ?>
        </div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 ml-auto mr-auto text-center">
				<h5 class="subtitle"><?php echo $mwt_options['info_subtitle']; ?></h5>
			</div>
		</div>
    <?php if( count( $mwt_options['info_items'] ) > 0 ) : ?>
    <br>
		<div class="row">
			<div class="col-sm-12 col-md-8 ml-auto mr-auto">
        <ol class="info-items">
          <?php foreach( $mwt_options['info_items'] as $item ) : ?>
          <li><?php echo $item; ?></li>
          <?php endforeach; ?>
        </ol>
			</div>
		</div>
    <?php endif; ?>
    <?php if( !empty( $mwt_options['info_btn_text'] ) ) : ?>
    <br>
		<div class="row">
			<div class="col-sm-12 col-md-10 ml-auto mr-auto text-center">
				<p>
          <a href="<?php echo $mwt_options['info_btn_url']; ?>" class="btn btn-primary btn-fill wow fadeInUp"><?php echo $mwt_options['info_btn_text']; ?></a>
        </p>
			</div>
		</div>
    <?php endif; ?>
    <div class="row">
      <div class="col-md-12 text-center partner-logo">
        <?php $partner_gallery = explode( ",", $mwt_options['partner_logo'] ); 
        $count = 1;
        foreach( $partner_gallery as $photo_id ) :
        ?>
          <span class="partner-<?php echo $count; ?>">
            <img src="<?php echo wp_get_attachment_url( $photo_id ); ?>" class="img-fluid">
          </span>
        <?php $count++; endforeach; ?>
      </div>
    </div>
	</div>
</div>

<?php endif; ?>