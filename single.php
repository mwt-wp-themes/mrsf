<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Adinaz
 */

get_header(); ?>



	<div id="primary" class="section section-white">
		<main id="main" class="container">

		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content', 'single' ); ?>
			
			<div class="row">
				<div class="col-md-10 ml-auto mr-auto">
            <hr>
            <div class="container">
							<?php
							// If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) :
								comments_template();
							endif;
							?>
            </div>
				</div>
			</div>
			<?php mwt_related_post(); ?>
			<?php
		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
