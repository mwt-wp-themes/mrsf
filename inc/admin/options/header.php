<?php

Redux::setSection( $opt_name, array(
    'title'  => __( 'Header', 'mwt' ),
    'id'     => 'mwt-header-options',
    'icon'   => 'el el-website',
    'fields' => array(
        array(
            'id'       => 'header_bg_image',
            'type'     => 'media',
            'title'    => __( 'Default Header Background', 'mwt' ),
            'url'      => true,
            'preview'  => true,
        ),
    )
) );