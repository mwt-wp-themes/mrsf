<?php


// create custom plugin settings menu
add_action('admin_menu', 'mwt_create_mjs_menu');

function mwt_create_mjs_menu() {
 
	//create new top-level menu
	add_menu_page('Data Dukungan', 'Dukungan', 'administrator', 'data_dukungan', 'mwt_display_data_dukungan_page' , 'dashicons-megaphone', 5 );
  

}

function mwt_display_data_dukungan_page() { ?>

    <style scoped>

        .button-success,
        .button-error,
        .button-warning,
        .button-secondary {
            color: white;
            border-radius: 4px;
            text-shadow: 0 1px 1px rgba(0, 0, 0, 0.2);
        }

        .button-success {
            background: rgb(28, 184, 65); /* this is a green */
        }

        .button-error {
            background: rgb(202, 60, 60); /* this is a maroon */
        }

        .button-warning {
            background: rgb(223, 117, 20); /* this is an orange */
        }

        .button-secondary {
            background: rgb(66, 184, 221); /* this is a light blue */
        }

    </style>

    <div class="wrap">
      <h1 class="wp-heading-inline">Data Dukungan</h1>
      <a href="post-new.php?post_type=dukungan" class="page-title-action">Add New</a>
      
      <?php if( !empty( $_GET['deleted'] ) ) : ?>
      <div id="setting-error-settings_updated" class="updated settings-error notice is-dismissible"> 
<p><strong>Data dukungan berhasil dihapus.</strong></p><button type="button" class="notice-dismiss"><span class="screen-reader-text">Tutup pemberitahuan ini.</span></button></div>
      <?php endif; ?>
      
      <hr>
      
      <table id="example" class="pure-table pure-table-horizontal" width="100%">
              <thead>
                  <tr>
                      <th width="30">ID</th>
                      <th>Tanggal</th>
                      <th>Nama</th>
                      <th>Usia</th>
                      <th>Email</th>
                      <th>Nomor HP</th>
                      <th>Polres</th>
                      <th>&nbsp;</th>
                  </tr>
              </thead>
          </table>
			
    </div>

  <script>
  jQuery(document).ready(function($) {
      var table = $('#example').DataTable({
          order: [[ 1, "desc" ]],
          ajax: {
              url: ajaxurl,
              data: {
                action: 'mrsf_get_data_dukungan'
              },
              dataSrc: 'data'
          },
          dom: 'Bfrtip',
          buttons: [
             {
                  extend: 'excel',
                  text: 'Export Excel',
                  className: 'btn btn-default',
                  exportOptions: {
                      columns: 'th:not(:last-child)'
                  }
              },
             {
                  extend: 'pdf',
                  text: 'Export PDF',
                  className: 'btn btn-default',
                  exportOptions: {
                      columns: 'th:not(:last-child)'
                  }
              }
          ],
          columns: [
              { 
                 "data": "id",
                 "render": function(data, type, row, meta){
                    if(type === 'display'){
                        data = '<a href="post.php?post=' + data + '&action=edit">' + data + '</a>';
                    }
                    return data;
                 }
              }, 
              { 
                "type": "date",
                "data": "tanggal" 
              },
              { "data": "nama" },
              { "data": "usia" },
              { "data": "email" },
              { "data": "hp" },
              { "data": "polres" },
              { 
                 "data": null,
                 "render": function(data, type, row, meta){
                    if(type === 'display'){
                        data = '';
                        data += '<a href="post.php?post='+row.id+'&action=edit" class="thickbox button-primary pure-button">Edit</a>';
                        data += '<button class="delbut button-error pure-button" onclick="del_post(' + row.id + ')">Hapus</a>';
                    }
                    return data;
                 }
              }, 
          ]
      });
        
  } );
    
    
  function del_post(post_id) {
    if( confirm( 'Anda yakin ingin menghapus data ini?' ) ) {
      jQuery.post(
        ajaxurl, 
        {
            'action': 'mrsf_hapus_data_dukungan',
            'id': post_id
        },
        function(response){
          window.location.replace('admin.php?page=data_dukungan&deleted=true');
        });
    }
  }
    
</script>

<?php } 