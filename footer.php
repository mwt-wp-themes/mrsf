<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package rensya
 */

global $mwt_options;
?>

	<footer class="footer">
		<div class="container">
			<div class="row">
				<nav class="footer-nav">
					<div class="social-area">
						<?php if( $mwt_options['social-facebook'] != '' ): ?>
							<a href="<?php echo $mwt_options['social-facebook']; ?>">
									<i class="fa fa-facebook" aria-hidden="true"></i>
							</a>
						<?php endif; ?>
						<?php if( $mwt_options['social-twitter'] != '' ): ?>
							<a href="<?php echo $mwt_options['social-twitter']; ?>">
									<i class="fa fa-twitter" aria-hidden="true"></i>
							</a>
						<?php endif; ?>
						<?php if( $mwt_options['social-google-plus'] != '' ): ?>
							<a href="<?php echo $mwt_options['social-google-plus']; ?>">
									<i class="fa fa-google-plus" aria-hidden="true"></i>
							</a>
						<?php endif; ?>
						<?php if( $mwt_options['social-linkedin'] != '' ): ?>
							<a href="<?php echo $mwt_options['social-facebook']; ?>">
									<i class="fa fa-linkedin" aria-hidden="true"></i>
							</a>
						<?php endif; ?>
						<?php if( $mwt_options['social-instagram'] != '' ): ?>
							<a href="<?php echo $mwt_options['social-instagram']; ?>">
									<i class="fa fa-instagram" aria-hidden="true"></i>
							</a>
						<?php endif; ?>
						<?php if( $mwt_options['social-tumblr'] != '' ): ?>
							<a href="<?php echo $mwt_options['social-tumblr']; ?>">
									<i class="fa fa-tumblr" aria-hidden="true"></i>
							</a>
						<?php endif; ?>
						<?php if( $mwt_options['social-youtube'] != '' ): ?>
							<a href="<?php echo $mwt_options['social-youtube']; ?>">
									<i class="fa fa-youtube" aria-hidden="true"></i>
							</a>
						<?php endif; ?>
						<?php if( $mwt_options['social-pinterest'] != '' ): ?>
							<a href="<?php echo $mwt_options['social-pinterest']; ?>">
									<i class="fa fa-pinterest" aria-hidden="true"></i>
							</a>
						<?php endif; ?>
						<?php if( $mwt_options['social-rss'] != '' ): ?>
							<a href="<?php echo $mwt_options['social-rss']; ?>">
									<i class="fa fa-rss" aria-hidden="true"></i>
							</a>
						<?php endif; ?>
					</div>
				</nav>
				<div class="credits ml-auto">
					<span class="copyright">
						<?php if( $mwt_options['copyright-text'] != '' ) {
							echo $mwt_options['copyright-text']; 
						} else { 
							echo 'Copyright &copy; ' . date('Y') . ' ' . get_bloginfo('name') . '. All Rights Reserved.'; 
						} ?>
					</span>
				</div>
			</div>
		</div>
	</footer>

	</div><!-- .wrapper -->

	<?php wp_footer(); ?>
          
  <script>
jQuery('#latest-post-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    items:1
});      
  </script>

</body>
</html>
