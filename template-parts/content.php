<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Adinaz
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="article">
			<div class="row">
					<div class="col-md-8 ml-auto mr-auto">						
							<div class="card card-blog card-plain text-center">
									<?php mwt_post_thumbnail(); ?>
									<div class="card-body">
											<header class="entry-header">
												<?php if ( 'post' === get_post_type() ) : ?>
												<div class="entry-meta card-category">
													<?php
														if ( 'post' === get_post_type() ) {
															/* translators: used between list items, there is a space after the comma */
															$categories_list = get_the_category_list( '</span><span class="label label-info main-tag">' );
															if ( $categories_list ) {
																/* translators: 1: list of categories. */
																printf( '<span class="cat-links label label-info main-tag">' . esc_html__( '%1$s', 'adinaz' ) . '</span>', $categories_list ); // WPCS: XSS OK.
															}
														}
													?>
												</div><!-- .entry-meta -->
												<?php
												endif; ?>
												<?php
												if ( is_singular() ) :
													the_title( '<h1 class="entry-title">', '</h1>' );
												else :
													the_title( '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark"><h3 class="card-title entry-title">', '</h3></a>' );?>
												<h6 class="title-uppercase"><?php mwt_posted_on(); ?></h6><?php
												endif; ?>
											</header><!-- .entry-header -->
											<div class="card-description">
													<div class="entry-content">
														<?php
														if( is_singular() ) :
															the_content( sprintf(
																wp_kses(
																	/* translators: %s: Name of current post. Only visible to screen readers */
																	__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'adinaz' ),
																	array(
																		'span' => array(
																			'class' => array(),
																		),
																	)
																),
																get_the_title()
															) );

															wp_link_pages( array(
																'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'adinaz' ),
																'after'  => '</div>',
															) );
														else:
														 the_excerpt();
														endif;
														?>
													</div><!-- .entry-content -->
											</div>
											<footer class="entry-footer">
												<?php mwt_entry_footer(); ?>
											</footer><!-- .entry-footer -->
									</div>
									<a href="<?php the_permalink(); ?>" class="btn btn-primary btn-round btn-sm">Baca Selengkapnya</a>
							</div>
					</div>
			</div>
	</div>
</article><!-- #post-<?php the_ID(); ?> -->
