<?php global $mwt_options; if( $mwt_options['enable_faqs'] == 1 ) : ?>
<div id="section-faqs" class="section section-white">
    <div class="container">
      <div class="row">
        <div class="col-md-8 ml-auto mr-auto text-center">
          <h2 class="title wow slideInDown"><?php echo $mwt_options['faqs_title']; ?></h2>
          <div class="space-top"></div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-8 col-12 ml-auto mr-auto">
            <div id="faq-accordion">
              <?php $wp_query = new WP_Query(array('post_type'=>'faqs', 'post_status'=>'publish', 'order' => 'ASC', 'nopaging' => true)); ?>
                <?php if ( $wp_query->have_posts() ) : ?>
                    <!-- the loop -->
                    <?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
                      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-12 card wow fadeIn" data-wow-duration="2s" data-wow-offset="25">
                        <div class="card-header" id="heading<?php the_ID(); ?>">
                          <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapse<?php the_ID(); ?>" aria-expanded="false" aria-controls="collapse<?php the_ID(); ?>"><?php the_title(); ?></button>
                          </h5>
                        </div>
                        <div id="collapse<?php the_ID(); ?>" class="collapse" aria-labelledby="heading<?php the_ID(); ?>" data-parent="#faq-accordion">
                          <div class="card-body">
                            <?php the_content(); ?>
                          </div>
                        </div>
                      </div>	
                    <?php endwhile; ?>
                    <!-- end of the loop -->
                <?php endif; ?>
              <?php wp_reset_postdata(); ?>
            </div>
        </div>
      </div>
    </div>
  </div>
<?php endif; ?>