<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Adinaz
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <div class="card">
    <div class="card-body">
      <div class="row">
          <div class="col-md-8">
            <header class="entry-header">
            <div class="card-category">
              <i class="fa fa-bookmark"></i>
              <?php
              if ( 'post' === get_post_type() ) {
                /* translators: used between list items, there is a space after the comma */
                $categories_list = get_the_category_list( esc_html__( ', ', 'rensya' ) );
                if ( $categories_list ) {
                  /* translators: 1: list of categories. */
                  printf( '<span class="main-tag">' . esc_html__( '%1$s', 'rensya' ) . '</span>', $categories_list ); // WPCS: XSS OK.
                }

                /* translators: used between list items, there is a space after the comma */
                //$tags_list = get_the_tag_list( '', esc_html_x( ', ', 'list item separator', 'rensya' ) );
                //if ( $tags_list ) {
                  /* translators: 1: list of tags. */
                  //printf( '<span class="tags-links">' . esc_html__( 'Tagged %1$s', 'rensya' ) . '</span>', $tags_list ); // WPCS: XSS OK.
                //}
              }
              ?>
            </div>
            <?php
            if ( is_singular() ) :
              the_title( '<h1 class="entry-title card-title">', '</h1>' );
            else :
              the_title( '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark"><h3 class="entry-title card-title">', '</h3></a>' );
            endif;

            if ( 'post' === get_post_type() ) : ?>
            <div class="entry-meta">
              <?php mwt_posted_on(); ?>
            </div><!-- .entry-meta -->
            <?php
            endif; ?>
          </header><!-- .entry-header -->
            <div class="entry-content card-description">
              <?php the_excerpt(); ?>
            </div>
            <footer class="entry-footer">
              <a href="<?php echo get_the_permalink(); ?>" class="btn btn-primary btn-round btn-sm">Read more</a>
            </footer><!-- .entry-footer -->
          </div>
          <div class="col-md-4">
            <a href="<?php the_permalink(); ?>">
              <?php if(has_post_thumbnail()): ?>
                <img class="img-rounded img-responsive" src="<?php echo get_the_post_thumbnail_url(); ?>">
              <?php else: ?>
                <img class="img-rounded img-responsive" src="https://via.placeholder.com/400x300">
              <?php endif; ?>
            </a>
          </div>
      </div>
    </div>
  </div>
</article><!-- #post-<?php the_ID(); ?> -->
