<?php
/**
 * Template name: Home
 */

get_header(); global $mwt_options; ?>

    <?php get_template_part('sections/section', 'rollout'); ?>
  
    <?php get_template_part('sections/section', 'info'); ?>

    <?php get_template_part('sections/section', 'form'); ?>

    <?php get_template_part('sections/section', 'about'); ?>

    <?php get_template_part('sections/section', 'faqs'); ?>

    <?php get_template_part('sections/section', 'blog'); ?>

<?php
get_footer();
