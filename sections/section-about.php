<?php if( $mwt_options['enable_about_us'] == 1 ) : 
$about_us_bg_image =  $mwt_options['about_us_subsection_bg_image']['url'];?>
<div id="section-about-us" class="section">
  <div class="container">
        <div class="row">
          <div class="col-md-10 ml-auto mr-auto">
            <h2 class="title wow slideInDown"><?php echo $mwt_options['about_us_title']; ?></h2>
            <div class="description wow fadeInDown"><?php echo $mwt_options['about_us_content']; ?></div>
          </div>
        </div>
    </div>
</div>

<div id="subsection-about-us" class="features-5 section-image" style="background-image: url('<?php echo $about_us_bg_image; ?>')">
  <div class="container">
      <div class="row">
        <div class="col-md-10 ml-auto mr-auto">
          <div class="row">
              <div class="col-md-4 col-sm-12 col-12">
                  <div class="nav nav-tabs flex-column nav-stacked" role="tablist" aria-orientation="vertical">
                    <a class="btn btn-outline-warning btn-round active wow rollIn" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true"><?php echo $mwt_options['about_us_collapse_1_title']; ?></a>
                    <a class="btn btn-outline-warning btn-round wow rollIn" data-wow-delay=".2s" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false"><?php echo $mwt_options['about_us_collapse_2_title']; ?></a>
                  </div>
              </div>
              <div class="col-md-8 col-sm-12 col-12">
                  <div class="tab-content wow lightSpeedIn" id="v-pills-tabContent" data-wow-delay=".5s">
                    <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab"><?php echo $mwt_options['about_us_collapse_1_content']; ?></div>
                    <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab"><?php echo $mwt_options['about_us_collapse_2_content']; ?></div>
                  </div>
              </div>
          </div>
        </div>	
      </div>
  </div>
</div>
<?php endif; ?>