<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Adinaz
 */

get_header(); ?>
<?php global $mwt_options; ?>

	<div id="primary" class="section">
		<main id="main" class="container">

		<?php
		if ( have_posts() ) : ?>

			<div class="row">
        <div class="col-sm-12 col-md-8">
				<?php
				$custom_post_types = array(
					'event', 'simple_product', 'faqs'
				);
				/* Start the Loop */
				while ( have_posts() ) : the_post();

					/*
					 * Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					$template_part = ( in_array( get_post_type(), $custom_post_types ) ) ? 'archive-' . get_post_type() : 'archive';
					get_template_part( 'template-parts/content', $template_part );

				endwhile; ?>
					<?php mwt_pagination(); ?>
				</div>
        <div class="col-sm-12 col-md-4">
          <?php get_sidebar(); ?>
        </div>
			</div>
			<?php
		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
