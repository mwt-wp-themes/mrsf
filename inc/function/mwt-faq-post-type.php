<?php

// Register Custom Post Type
function mwt_faq_post_type() {

	$labels = array(
		'name'                  => _x( 'FAQs', 'Post Type General Name', 'rensya' ),
		'singular_name'         => _x( 'FAQ', 'Post Type Singular Name', 'rensya' ),
		'menu_name'             => __( 'FAQs', 'rensya' ),
		'name_admin_bar'        => __( 'FAQ', 'rensya' ),
		'archives'              => __( 'FAQ Archives', 'rensya' ),
		'attributes'            => __( 'FAQ Attributes', 'rensya' ),
		'parent_item_colon'     => __( 'Parent Item:', 'rensya' ),
		'all_items'             => __( 'All FAQs', 'rensya' ),
		'add_new_item'          => __( 'Add New FAQ', 'rensya' ),
		'add_new'               => __( 'Add New', 'rensya' ),
		'new_item'              => __( 'New FAQ', 'rensya' ),
		'edit_item'             => __( 'Edit FAQ', 'rensya' ),
		'update_item'           => __( 'Update FAQ', 'rensya' ),
		'view_item'             => __( 'View Item', 'rensya' ),
		'view_items'            => __( 'View Items', 'rensya' ),
		'search_items'          => __( 'Search Item', 'rensya' ),
		'not_found'             => __( 'Not found', 'rensya' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'rensya' ),
		'featured_image'        => __( 'Featured Image', 'rensya' ),
		'set_featured_image'    => __( 'Set featured image', 'rensya' ),
		'remove_featured_image' => __( 'Remove featured image', 'rensya' ),
		'use_featured_image'    => __( 'Use as featured image', 'rensya' ),
		'insert_into_item'      => __( 'Insert into item', 'rensya' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'rensya' ),
		'items_list'            => __( 'Items list', 'rensya' ),
		'items_list_navigation' => __( 'Items list navigation', 'rensya' ),
		'filter_items_list'     => __( 'Filter items list', 'rensya' ),
	);
	$args = array(
		'label'                 => __( 'FAQ', 'rensya' ),
		'description'           => __( 'Frequently Asked Questions', 'rensya' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-category',
		'show_in_admin_bar'     => false,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'rewrite'               => false,
		'capability_type'       => 'page',
	);
	register_post_type( 'faqs', $args );

}
add_action( 'init', 'mwt_faq_post_type', 0 );