<?php

Redux::setSection( $opt_name, array(
    'title'  => __( 'General', 'mwt' ),
    'id'     => 'mwt-general-options',
    'icon'   => 'el el-dashboard',
    'fields' => array(
        array(
            'id'       => 'logo',
            'type'     => 'media',
            'title'    => __( 'Logo', 'mwt' ),
            'url'      => true,
            'preview'  => true,
        ),
        array(
            'id'       => 'typography',
            'type'        => 'typography', 
            'title'       => __('Body Typography', 'mwt'),
            'google'      => true, 
            'font-backup' => true,
            'font-style'  => false, 
            'line-height' => false,
            'text-align'  => false,
            'output'      => array('body', 'p'),
            'units'       =>'px',
            'subtitle'    => __('Typography option with each property can be called individually.', 'mwt'),
            'default'     => array(
                'color'       => '#66615b', 
                'font-family' => 'Montserrat', 
                'google'      => true,
                'font-size'   => '14px', 
                'font-weight' => '400',
            ),
        ),
        array(
            'id'       => 'heading_typography',
            'type'        => 'typography', 
            'title'       => __('Heading Typography', 'mwt'),
            'google'      => true, 
            'output'      => array('h1,h2,h3,h4'),
            'font-backup' => true,
            'font-size'   => false,
            'color'   => false,
            'font-style'  => false, 
            'line-height' => false,
            'text-align'  => false,
            'units'       =>'px',
            'subtitle'    => __('Typography option with each property can be called individually.', 'mwt'),
        ),
        array(
            'id'       => 'analytics-code',
            'type'     => 'textarea',
            'title'    => __('Analytics Code', 'mwt'), 
            'desc'     => __('Paste your analytics code like google analytics here.', 'mwt'),
            'validate' => 'html_custom',
            'default'  => ''
        ),
        array(
            'id'       => 'partner_logo',
            'type'     => 'gallery',
            'title'    => __( 'Partner Logo', 'mwt' ),
        ),
    )
) );