<?php

Redux::setSection( $opt_name, array(
    'title'      => __( 'Contact', 'rensya' ),
    'desc'       => __( '', 'rensya' ),
    'id'         => 'mwt-contacts-options',
    'icon'       => 'el el-bullhorn',
    'fields'     => array(
        array(
            'id'       => 'contact-company-name',
            'type'     => 'text',
            'title'    => __( 'Nama Perusahaan', 'rensya' ),
        ),
        array(
            'id'       => 'contact-address',
            'type'     => 'textarea',
            'title'    => __( 'Alamat', 'rensya' ),
        ),
        array(
            'id'       => 'contact-email',
            'type'     => 'text',
            'title'    => __( 'Email', 'rensya' ),
            'validate' => 'email',
        ),
        array(
            'id'       => 'contact-phone-1',
            'type'     => 'text',
            'title'    => __( 'Telepon', 'rensya' ),
        ),
        array(
            'id'       => 'contact-fax',
            'type'     => 'text',
            'title'    => __( 'Fax', 'rensya' ),
        ),
        array(
            'id'       => 'contact-phone-2',
            'type'     => 'text',
            'title'    => __( 'Customer Care', 'rensya' ),
        ),
        array(
            'id'       => 'contact-phone-3',
            'type'     => 'text',
            'title'    => __( 'Technical Support', 'rensya' ),
        ),
    )
) );