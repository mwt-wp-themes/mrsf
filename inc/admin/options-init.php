<?php

    /**
     * For full documentation, please visit: http://docs.reduxframework.com/
     * For a more extensive sample-config file, you may look at:
     * https://github.com/reduxframework/redux-framework/blob/master/sample/sample-config.php
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }

    // This is your option name where all the Redux data is stored.
    $opt_name = "mwt_options";

    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        'opt_name' => $opt_name,
        'dev_mode' => FALSE,
        'use_cdn' => FALSE,
        'display_name' => 'Theme Options',
        'display_version' => '1.0.0',
        'page_slug' => $opt_name,
        'page_title' => 'Theme Options',
        'update_notice' => FALSE,
        // 'intro_text' => '',
        // 'footer_text' => '',
        'admin_bar' => FALSE,
        'menu_type' => 'submenu',
        'menu_title' => 'Theme Options',
        'allow_sub_menu' => TRUE,
        'page_parent' => 'themes.php',
        'customizer' => TRUE,
        'default_mark' => '*',
        'google_api_key' => 'AIzaSyBF-PcYoLWQVs0ObXrRr8aNR-H2H2W_GjI',
        'class' => 'mwt-redux',
        'hints' => array(
            'icon' => 'el el-bullhorn',
            'icon_position' => 'right',
            'icon_size' => 'normal',
            'tip_style' => array(
                'color' => 'red',
                'rounded' => '1',
                'style' => 'bootstrap',
            ),
            'tip_position' => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect' => array(
                'show' => array(
                    'effect' => 'fade',
                    'duration' => '500',
                    'event' => 'mouseover',
                ),
                'hide' => array(
                    'effect' => 'fade',
                    'duration' => '500',
                    'event' => 'mouseleave unfocus',
                ),
            ),
        ),
        'output' => TRUE,
        'output_tag' => TRUE,
        'settings_api' => TRUE,
        'cdn_check_time' => TRUE,
        'compiler' => TRUE,
        'page_permissions' => 'manage_options',
        'show_import_export' => true,
        'database' => 'options',
        'transient_time' => '3600',
        'network_sites' => FALSE,
        'system_info' => FALSE,
        'hide_reset' => TRUE,
    );

    // SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
    $args['share_icons'][] = array(
        'url'   => 'https://www.facebook.com/JktHosting',
        'title' => 'Like us on Facebook',
        'icon'  => 'el el-facebook'
    );
    $args['share_icons'][] = array(
        'url'   => 'http://twitter.com/JktHosting',
        'title' => 'Follow us on Twitter',
        'icon'  => 'el el-twitter'
    );

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */

    /*
     * ---> START HELP TABS
     */

    $tabs = array(
        array(
            'id'      => 'redux-help-tab-1',
            'title'   => __( 'Theme Information 1', 'rensya' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'rensya' )
        ),
        array(
            'id'      => 'redux-help-tab-2',
            'title'   => __( 'Theme Information 2', 'rensya' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'rensya' )
        )
    );
    Redux::setHelpTab( $opt_name, $tabs );

    // Set the help sidebar
    $content = __( '<p>This is the sidebar content, HTML is allowed.</p>', 'rensya' );
    Redux::setHelpSidebar( $opt_name, $content );


    /*
     * <--- END HELP TABS
     */


    /*
     *
     * ---> START SECTIONS
     *
     */

    require_once __DIR__ . ('/options/general.php');
    require_once __DIR__ . ('/options/frontpage.php');
    require_once __DIR__ . ('/options/header.php');
    require_once __DIR__ . ('/options/footer.php');
    //require_once __DIR__ . ('/options/contacts.php');
    require_once __DIR__ . ('/options/socials.php');
    //require_once __DIR__ . ('/options/customs.php');

    /*
     * <--- END SECTIONS
     */
